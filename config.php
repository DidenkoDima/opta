<?php
// HTTP
define('HTTP_SERVER', 'http://Opta/');

// HTTPS
define('HTTPS_SERVER', 'http://Opta/');

// DIR
define('DIR_APPLICATION', 'E:/OSPanel/domains/Opta/catalog/');
define('DIR_SYSTEM', 'E:/OSPanel/domains/Opta/system/');
define('DIR_IMAGE', 'E:/OSPanel/domains/Opta/image/');
define('DIR_LANGUAGE', 'E:/OSPanel/domains/Opta/catalog/language/');
define('DIR_TEMPLATE', 'E:/OSPanel/domains/Opta/catalog/view/theme/');
define('DIR_CONFIG', 'E:/OSPanel/domains/Opta/system/config/');
define('DIR_CACHE', 'E:/OSPanel/domains/Opta/system/storage/cache/');
define('DIR_DOWNLOAD', 'E:/OSPanel/domains/Opta/system/storage/download/');
define('DIR_LOGS', 'E:/OSPanel/domains/Opta/system/storage/logs/');
define('DIR_MODIFICATION', 'E:/OSPanel/domains/Opta/system/storage/modification/');
define('DIR_UPLOAD', 'E:/OSPanel/domains/Opta/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'Opta');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
