<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo $title; ?>
    </title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" /> -->
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <!-- front_style=-=-=- -->
    <script src="catalog/view/theme/default/js/main.js" type="text/javascript"></script>
    <link rel="stylesheet" href="catalog/view/theme/default/css/normilize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <link rel="stylesheet" href="catalog/view/theme/default/css/font-awesome.min.css">
    <link rel="stylesheet" href="catalog/view/theme/default/css/main.css">
    <!-- frjnt_end_style=-=-=-= -->
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>

<body class="<?php echo $class; ?>">
    <div id="wrapper">
      <div class="wel_modal">
          <div id="welcome_load">
            <div id="header_load">
              <form class="clear" action="/index.php?route=account/login" method="post" enctype="multipart/form-data">
              <div class="title_n">
                <div class="h4 item11"><span>Вход</span></div>
                <div class="btn_close">
                    <span class="line1"></span>
                    <span class="line2"></span>
                </div>
              </div>
                <div class="login_wr">
                    <div class="login-form">
                        <div class="input-email">
                            <p><span class="title_pop">E-Mail</span><span class="color">*</span></p>
                            <input type="hidden" name="redir" value="">  
                            <input type="text" name="email" value="" class="form-control">
                        </div>
                        <div class="input-password">
                            <p><span class="title_pop">Пароль</span><span class="color">*</span></p>
                            <input type="password" name="password" value="" class="form-control">
                        </div>
                        <div class="buttons ">
                            <p>
                               <a class="pass-forgotten" href="/index.php?route=account/forgotten">Забыли пароль?</a>
                            </p>
                            <div class="m_s-btn">
                                <!-- <input type="submit" value="Войти" class="btn_all" id="s-btn"> -->
                                <button type="submit" class="btn_all" id="s-btn">Войти</button>
                                <label for="s-btn"></label>
                             <!--    <button class="canc" type="reset">Отмена</button> -->
                            </div>     
                        </div>
                    </div>

                </div>
                <div class="razd"><span>или</span></div>
                <div class="main_social">
                <div class="soc_link">
                  <p>Войти через соц.сети</p>
                  <span><a href="/socnetauth2/facebook.php?first=1"><img src="/image/new_img/face.png" alt="soc"></a></span>
                  <span><a href="/socnetauth2/twitter.php?first=1"><img src="/image/new_img/twiter.png" alt="soc"></a></span>
                  <span><a href="/socnetauth2/vkontakte.php?first=1"><img src="/image/new_img/vk.png" alt="soc"></i></a></span>
                  <span><a href="/socnetauth2/gmail.php?first=1"><img src="/image/new_img/google.png" alt="soc"></i></a></span>
                </div>
                <div class="reg_main">
                  <span class="reg"><span>Нет учетной записи?</span><a class="zareg" href="index.php?route=account/simpleregister">Зарегистрироваться</a></span>
                </div>
                </div>
               </form>
                <div class="soc_login">
                  <div id="content-top"></div>  
                </div>
              </div>
          </div>
      </div>
        <div class="header-top">
            <div class="left_block_header">
                <div id="logo">
                    <?php if ($logo) { ?>
                    <a href="<?php echo $home; ?>">
                      <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                    </a>
                    <?php } else { ?>
                      <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                    <?php } ?>
                </div>
                <div id="telephones">
                    <div class="telephone_1">
                        <div class="telephone_1_title">
                            Харьков
                        </div>
                        <div class="telephone_1_wr pad">
                            <span><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a><i class="click_down">
                              <img src="/catalog/view/theme/default/img/arrow_down.png" alt=""></i></span>
                            <div class="telephone_1_drop">
                                <ul>
                                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
                                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="telephone_2">
                        <div class="telephone_2_title">
                            Киев
                        </div>
                        <div class="telephone_2_wr pad">
                            <span><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a><i class="click_down">
                              <img src="/catalog/view/theme/default/img/arrow_down.png" alt=""></i></span>
                            <div class="telephone_2_drop">
                                <ul>
                                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
                                    <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right_block_header">
                <div class="wr_bl">
                    <div class="wish_and_comp">
                        <a href="<?php echo $compare; ?>" id="compare-total" >
                            <div>
                                <img src="/catalog/view/theme/default/img/head_compare.png" alt="<?php echo $compare; ?>">
                                <span class="icon_count" id="compare_icon"><?php echo $text_compare; ?></span>
                            </div>
                            <p><?php echo $text_compare_1; ?></p>
                        </a>
                        <a href="<?php echo $wishlist; ?>" id="wishlist-total">
                            <div>
                                <img src="/catalog/view/theme/default/img/head_wish.png" alt="<?php echo $text_wishlist_1; ?>">
                                <span class="icon_count" id="wish_icon"><?php echo $text_wishlist; ?></span>
                            </div>
                            <p><?php echo $text_wishlist_1; ?></p>
                        </a>
                    </div>
                        <?php echo $cart; ?>
                    <div class="login">
                        <a  href="javascript:void(0)">
                            <div>
                                <img src="/catalog/view/theme/default/img/login.png" alt="<?php echo $text_account; ?>">
                            </div>
                            <p><?php echo $text_account; ?></p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php /*<nav id="top">
            <div class="container">
                <?php echo $currency; ?>
                <?php /* echo $language;  ?>
                <?php echo $menu; ?>
                <div id="top-links" class="nav pull-right">
                    <ul class="list-inline">
                        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span>
                        </li>
                        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php if ($logged) { ?>
                                <li>
                                    <a href="<?php echo $account; ?>">
                                        <?php echo $text_account; ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $order; ?>">
                                        <?php echo $text_order; ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $transaction; ?>">
                                        <?php echo $text_transaction; ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $download; ?>">
                                        <?php echo $text_download; ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $logout; ?>">
                                        <?php echo $text_logout; ?>
                                    </a>
                                </li>
                                <?php } else { ?>
                                <li>
                                    <a href="<?php echo $register; ?>">
                                        <?php echo $text_register; ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $login; ?>">
                                        <?php echo $text_login; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
                        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
                        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
                    </ul>
                </div>
            </div>
        </nav> */ ?>

          <div id="block_menu">
            <?php if ($categories) { ?>
              <div id="menu">
                <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                </div>
                <ul class="menu">
                  <?php foreach ($categories as $category) { ?>
                    <?php if ($category['children']) { ?>
                      <li>
                        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?>
                          <img src="/catalog/view/theme/default/img/arrow_menu.png" alt="arrow_menu">
                        </a>
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                          <div class="menu_drop">
                            <ul>
                              <?php foreach ($children as $child) { ?>
                                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                              <?php } ?>
                            </ul>
                          </div>
                        <?php } ?>
                      </li>
                    <?php } ?>
                  <?php } ?>
                </ul>
              </div>
            <?php } ?>

            <?php echo $search; ?>



<!--             <div id="price_click">
  <a href="javascript:void(0)">
    <span>грн (НДС)<img src="/catalog/view/theme/default/img/arrow_menu.png" alt="arrow_menu"></span>
  </a>
  <div class="price_click_drop" style="display: none;">
    <a href="#">грн (НДС)</a>
    <a href="#">грн (без НДС)</a>
  </div>
  <div class="title">
    Стоимость товара НДС
  </div>
</div> -->
            <?php echo $currency; ?>
          </div>




       <?php /* <header>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                       <?php /* <div id="logo">
                            <?php if ($logo) { ?>
                            <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                            <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                            <?php } ?>
                        </div>
                    </div>  ?> 
                    <div class="col-sm-5">
                        <?php echo $search; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $cart; ?>
                    </div>
                </div>
            </div>
        </header> */ ?>
        <?php /* if ($categories) { ?>
        <div class="container">
            <nav id="menu" class="navbar">
                <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <?php foreach ($categories as $category) { ?>
                        <?php if ($category['children']) { ?>
                        <li class="dropdown">
                            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown">
                                <?php echo $category['name']; ?>
                            </a>
                            <div class="dropdown-menu">
                                <div class="dropdown-inner">
                                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                    <ul class="list-unstyled">
                                        <?php foreach ($children as $child) { ?>
                                        <li>
                                            <a href="<?php echo $child['href']; ?>">
                                                <?php echo $child['name']; ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </div>
                                <a href="<?php echo $category['href']; ?>" class="see-all">
                                    <?php echo $text_all; ?>
                                    <?php echo $category['name']; ?>
                                </a>
                            </div>
                        </li>
                        <?php } else { ?>
                        <li>
                            <a href="<?php echo $category['href']; ?>">
                                <?php echo $category['name']; ?>
                            </a>
                        </li>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </div>
        <?php } */ ?>
