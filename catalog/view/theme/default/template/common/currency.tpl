<?php /* if (count($currencies) > 1) { ?>
<div id="price_click">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-currency">
    <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
    <?php foreach ($currencies as $currency) { ?>
      <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>
       <?php echo $currency['symbol_left']; ?>
      <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>
        <?php echo $currency['symbol_right']; ?>
      <?php } ?>
    <?php } ?>
    <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_currency; ?></span> <i class="fa fa-caret-down"></i>
  </button>
    <ul class="dropdown-menu price_click_drop">
      <?php foreach ($currencies as $currency) { ?>
      <?php if ($currency['symbol_left']) { ?>
      <li><button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?> <?php echo $currency['title']; ?></button></li>
      <?php } else { ?>
      <li><button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?> <?php echo $currency['title']; ?></button></li>
      <?php } ?>
      <?php } ?>
    </ul>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } */ ?>
<?php if (count($currencies) > 1) { ?>
<div id="price_click">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-currency">

    <a href="javascript:void(0)">
      <span>
        <?php foreach ($currencies as $currency) { ?>
          <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>
           <?php echo $currency['symbol_left']; ?>
          <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>
            <?php echo $currency['symbol_right']; ?>
          <?php } ?>
        <?php } ?> 
        <?php echo $text_currency; ?>
        <img src="/catalog/view/theme/default/img/arrow_menu.png" alt="arrow_menu"></span>
    </a>
    <div class="price_click_drop" style="display: none;">
      <?php foreach ($currencies as $currency) { ?>
        <?php if ($currency['symbol_left']) { ?>
          <a href="javascript:void(0)" name="<?php echo $currency['code']; ?>" class="currency-select">
            <?php echo $currency['symbol_left']; ?>
             <?php echo $currency['title']; ?>
          </a>
        <?php } else { ?>
          <a href="javascript:void(0)" class="currency-select" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?> <?php echo $currency['title']; ?>
          </a>
        <?php } ?>
      <?php } ?>
    </div>
    <div class="title">
      Стоимость товара c НДС
    </div>

  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php }  ?>