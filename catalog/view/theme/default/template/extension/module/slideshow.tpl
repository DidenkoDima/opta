<?php /*<div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
$('#slideshow<?php echo $module; ?>').owlCarousel({
	items: 6,
	autoPlay: 3000,
	singleItem: true,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: true
});
--></script> */ ?>

<div id="main_slider">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php foreach ($banners as $banner) { ?>
        <?php if ($banner['link']) { ?>
        <div class="swiper-slide">
          <a href="<?php echo $banner['link']; ?>">
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
          </a>
        </div>
        <?php } else { ?>
        <div class="swiper-slide">
           <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
        </div>
        <?php } ?>
        <?php } ?>
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
    </div>

  </div>
  <script>
    var swiper1 = new Swiper('#main_slider .swiper-container', {
      spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 8000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '#main_slider .swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '#main_slider .swiper-button-next',
        prevEl: '#main_slider .swiper-button-prev',
      },
    });
  </script>
<!--   <div class="advantages">
  <div class="advantages_child">
    <div>
      <img src="/catalog/view/theme/default/img/icon_1.png" alt="">
    </div>
    <div>
      <p>Работаем 24/7</p>
      <p>
        <span>24 часа в сутки</span>
        <span>7 дней в неделю</span>
      </p>
    </div>
  </div>
  <div class="advantages_child">
    <div>
      <img src="/catalog/view/theme/default/img/icon_2.png" alt="">
    </div>
    <div>
      <p>Быстрая доставка</p>
      <p>
        <span>отправка на следующий</span>
        <span>рабочий день</span>
      </p>
    </div>
  </div>
  <div class="advantages_child">
    <div>
      <img src="/catalog/view/theme/default/img/icon_3.png" alt="">
    </div>
    <div>
      <p>Удобная оплата</p>
      <p>
        <span>банковская карта, счет,</span>
        <span>наложенный платеж</span>
      </p>
    </div>
  </div>
  <div class="advantages_child">
    <div>
      <img src="/catalog/view/theme/default/img/icon_4.png" alt="">
    </div>
    <div>
      <p>Помощь в подборе</p>
      <p>
        консультации специалистов
      </p>
    </div>
  </div>
</div> -->