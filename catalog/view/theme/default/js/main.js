document.addEventListener('DOMContentLoaded',function(){
   var currency_btn = document.querySelector('#price_click'),
       currency_drop = document.querySelector('.price_click_drop');
   
   currency_btn.addEventListener('click',function(){
      if (currency_drop.style.display === "none") {
          currency_drop.style.display = "block";
      } else {
          currency_drop.style.display = "none";
      }
   });
   currency_btn.addEventListener('mouseleave',function(){
      currency_drop.style.display = "none";
   });

   // popup-=-=
   var header_load = $('#header_load'),
       hov_p = $('#background_site');
   $('.login').on('click', function() {
       $(header_load).toggleClass('act');
       $(hov_p).fadeIn(300);
   });
   $('.btn_close').on('click', function() {
       $(header_load).removeClass('act');
       $(hov_p).fadeOut(300);
   });
   $(hov_p).on('click', function() {
       $(header_load).removeClass('act');
       $(this).fadeOut(300);
   });
   // $('.contact-m').on('click', '.callback', function() {
   //     $('nav.mobile .close').click();
   // });
});