<?php
// Entry
$_['entry_review_title'] = 'Заголовок відгуку';
$_['entry_add_pros'] = 'Додати переваги';
$_['entry_add_cons'] = 'Додати недоліки';
$_['entry_review_image'] = 'Додати зображення в презентацію (max %s):';
$_['entry_recommend_product'] = 'Порекомендували б ви цей товар другу?';

// Text
$_['text_reply'] = 'Відповісти';
$_['text_pros'] = 'Переваги';
$_['text_cons'] = 'Недоліки';
$_['text_on'] = 'від';
$_['text_yes'] = 'Так';
$_['text_no'] = 'Ні';
$_['text_average_review'] = 'Усього:';
$_['text_product_review'] = 'Відгуки про %s';
$_['text_general_avarage'] = 'Середній рейтинг:';
$_['text_count_recommend_product'] = '<b>%s из %s (%s)</b> рецензентів<br /> порекомендували б цей товар другу.';
$_['text_general_count_mark'] = '<b>Усього відгуків:</b> %s<br />Напишіть свій відгук про цей товар.';
$_['text_report_abuse'] = 'Повідомити про порушення';
$_['text_report_it'] = 'Повідомити';
$_['text_other_reason'] = 'Інше (напишіть нижче)';
$_['text_please_wait'] = 'Зачекайте будь ласка!';
$_['text_helpfull_percentage'] = 'Чи був цей відгук корисним? <button class="vote_yes" data-vote="1" data-review-id="%s"> Так </button> <button class="vote_no" data-vote="0" data-review-id="%s"> Ні </button>%s знайшли цей відгук корисним.';
$_['text_helpfull_numerically'] = 'Чи був цей відгук корисним? <button class="vote_yes" data-vote="1" data-review-id="%s"> Так </button> <button class="vote_no" data-vote="0" data-review-id="%s"> Ні </button>%s з %s людей вважають цей відгук корисним.';
$_['text_share_title'] = '%s відгуків з %s';
$_['text_success_helpfull_percentage_yes'] = 'На вашу думку, це корисно. %s знайшов цей відгук корисним.';
$_['text_success_helpfull_percentage_no'] = 'На вашу думку, це марно. %s знайшов цей відгук корисним.';
$_['text_success_helpfull_numerically_yes'] = 'На вашу думку, це корисно. %s з %s знайшли цей відгук корисним.';
$_['text_success_helpfull_numerically_no'] = 'На вашу думку, це марно. %s з %s знайшли цей відгук корисним.';
$_['text_report_abuse_success'] = 'Ваш звіт успішно відправлений. Дякуємо.';
$_['text_sort'] = 'Сортувати за:';
$_['text_default'] = 'За замовчуванням';
$_['text_rating_desc'] = 'Рейтинг (від більшого)';
$_['text_rating_asc'] = 'Рейтинг (від меншого)';
$_['text_helpfull_desc'] = 'Корисність (від більшого)';
$_['text_helpfull_asc'] = 'Корисність (від меншого)';
$_['text_date_added_desc'] = 'Дата (останні)';
$_['text_date_added_asc'] = 'Дата (давні)';
$_['text_upload'] = 'Ваш файл був успішно завантажений!';

// Buttons
$_['button_write_review'] = 'Написати відгук';

// Error
$_['error_filename'] = 'Назва файлу повинна бути від 3 до 64 символів!';
$_['error_filetype'] = 'Невірний тип файлу!';
$_['error_upload'] = 'Похобка завантаження.';
$_['error_logged_guest_rate'] = 'Ви повинні авторизуватися, щоб оцінити цей продукт!';
$_['error_logged_helpfull'] = 'Ви повинні бути зареєстровані, якщо хочете проголосувати за відгук!';
$_['error_logged_report_abuse'] = 'Ви повинні увійти в систему, щоб повідомити про порушення!';
$_['error_report_abuse'] = 'Виберіть заголовок звіту!';
$_['error_def_report_abuse'] = 'Будь ласка, вкажіть причину звіту!';
$_['error_already_helpfull'] = 'Ви вже проголосували!';
$_['error_helpfull'] = 'Голосування не враховувалося. Будь ласка, спробуйте ще раз пізніше!';
$_['error_pros_cons_limit'] = 'Плюси і мінуси повинні мати кількість символів між від %s до %s!';
$_['error_purchase_product'] = 'Щоб написати відгук, ви повинні спочатку купити цей продукт!';
$_['error_already_review_product'] = 'Ви вже писали відгук про цей товар!';
$_['error_review_title'] = 'Тема відкликання повинен бути від 3 до 40 символів!';