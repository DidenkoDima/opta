<?php
// Entry
$_['entry_review_title'] = 'Заголовок отзыва';
$_['entry_add_pros'] = 'Добавить достоинства';
$_['entry_add_cons'] = 'Добавить недостатки';
$_['entry_review_image'] = 'Добавить изображение в презентацию (max %s):';
$_['entry_recommend_product'] = 'Порекомендовали бы вы этот товар другу?';

// Text
$_['text_reply'] = 'Ответить';
$_['text_pros'] = 'Преимущества';
$_['text_cons'] = 'Недостатки';
$_['text_on'] = 'от';
$_['text_yes'] = 'Да';
$_['text_no'] = 'Нет';
$_['text_average_review'] = 'Всего:';
$_['text_product_review'] = 'Отзывы о %s';
$_['text_general_avarage'] = 'Средний рейтинг:';
$_['text_count_recommend_product'] = '<b>%s из %s (%s)</b> рецензентов<br /> порекомендовали бы этот товар другу.';
$_['text_general_count_mark'] = '<b>Всего отзывов:</b> %s<br />Напишите свой отзыв об этом товаре.';
$_['text_report_abuse'] = 'Сообщить о нарушении';
$_['text_report_it'] = 'Сообщить';
$_['text_other_reason'] = 'Другое (напишите ниже)';
$_['text_please_wait'] = 'Подождите пожалуйста!';
$_['text_helpfull_percentage'] = 'Был ли этот отзыв полезен? <button class="vote_yes" data-vote="1" data-review-id="%s"> Да </button> <button class="vote_no" data-vote="0" data-review-id="%s"> Нет </button>%s нашли этот отзыв полезным.';
$_['text_helpfull_numerically'] = 'Был ли этот отзыв полезен? <button class="vote_yes" data-vote="1" data-review-id="%s"> Да </button> <button class="vote_no" data-vote="0" data-review-id="%s"> Нет </button>%s из %s людей считают этот отзыв полезным.';
$_['text_share_title'] = '%s отзывов из %s';
$_['text_success_helpfull_percentage_yes'] = 'По вашему мнению, это полезно. %s нашел этот отзыв полезным.';
$_['text_success_helpfull_percentage_no'] = 'По вашему мнению, это бесполезно. %s нашел этот отзыв полезным.';
$_['text_success_helpfull_numerically_yes'] = 'По вашему мнению, это полезно. %s из %s нашли этот отзыв полезным.';
$_['text_success_helpfull_numerically_no'] = 'По вашему мнению, это бесполезно. %s из %s нашли этот отзыв полезным.';
$_['text_report_abuse_success'] = 'Ваш отчет успешно отправлен. Спасибо.';
$_['text_sort'] = 'Сортировать по:';
$_['text_default'] = 'По умолчанию';
$_['text_rating_desc'] = 'Рейтинг (от большего)';
$_['text_rating_asc'] = 'Рейтинг (от меньшего)';
$_['text_helpfull_desc'] = 'Полезность (от большего)';
$_['text_helpfull_asc'] = 'Полезность (от меньшего)';
$_['text_date_added_desc'] = 'Дата (последние)';
$_['text_date_added_asc'] = 'Дата (давние)';
$_['text_upload'] = 'Ваш файл был успешно загружен!';

// Buttons
$_['button_write_review'] = 'Написать отзыв';

// Error
$_['error_filename'] = 'Имя файла должно быть от 3 до 64 символов!';
$_['error_filetype'] = 'Invalid file type!';
$_['error_upload'] = 'Неверный тип файла!';
$_['error_logged_guest_rate'] = 'Вы должны авторизоваться, чтобы оценить этот продукт!';
$_['error_logged_helpfull'] = 'Вы должны быть зарегистрированы, если хотите проголосовать за отзыв.!';
$_['error_logged_report_abuse'] = 'Вы должны войти в систему, чтобы сообщить о нарушении!';
$_['error_report_abuse'] = 'Выберите заголовок отчета!';
$_['error_def_report_abuse'] = 'Пожалуйста, укажите причину отчета!';
$_['error_already_helpfull'] = 'Вы уже проголосовали!';
$_['error_helpfull'] = 'Голосование не учитывалось. Пожалуйста, повторите попытку позже!';
$_['error_pros_cons_limit'] = 'Плюсы и минусы должны иметь количество символов между от %s до %s!';
$_['error_purchase_product'] = 'Чтобы написать отзыв, вы должны сначала купить этот продукт!';
$_['error_already_review_product'] = 'Вы уже писали отзыв об этом товаре!';
$_['error_review_title'] = 'Заголовок отзыва должен быть от 3 до 40 символов!';