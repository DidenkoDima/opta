<?php
// Heading
$_['heading_title']       = 'Advanced Product Reviews';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Advanced Product Reviews!';
$_['text_store']          = 'Store:';
$_['text_list']           = 'List';
$_['text_grid']           = 'Grid';
$_['text_yes_logged']     = 'Yes, only logged customer';
$_['text_yes_all']        = 'Yes, customers and guests';

// Entry
$_['entry_module_status']               = 'Module status';
$_['entry_notify_status']               = 'New reviews notification';
$_['entry_notify_email']                = 'Send emails to';
$_['entry_notification']                = 'Message notification';
$_['entry_rating_guest']                = 'Guests can\'t rate the products';
$_['entry_predefined_pros_cons_status'] = 'Show predefined Pros/Cons field';
$_['entry_pros_status']                = 'Show Pros field';
$_['entry_cons_status']                = 'Show Cons field';
$_['entry_report_abuse_status']        = 'Show Report abuse';
$_['entry_helpfulness_status']         = 'Show helpfulness';
$_['entry_total_rating_status']        = 'Show average rating of review';
$_['entry_report_abuse_guest']         = 'Guests can\'t Report abuse';
$_['entry_helpfulness_guest']          = 'Guests can\'t helpfulness';
$_['entry_helpfulness_type']           = 'Display type helpfulness';
$_['entry_autoapprove']                = 'Auto approve reviews';
$_['entry_autoapprove_rating']         = 'Rating stars';
$_['entry_appearance_type']            = 'Appearance type';
$_['entry_appearance_customer_rating'] = 'Appearance type of customer rating';
$_['entry_sort_status']                = 'Sort list reviews';
$_['entry_image_status']               = 'Adding an image to the review';
$_['entry_image_limit']                = 'Limit images that can add a user';
$_['entry_image_thumb']                = 'Review image thumb size';
$_['entry_image_popup']                = 'Review image popup size';
$_['entry_comment_status']             = 'Show a comment reply for review';
$_['entry_comment_author']             = 'Comment author';
$_['entry_comment_image_status']       = 'Images for a comment';
$_['entry_colorbox_status']            = 'Force use "Colorbox" to the popup image';
$_['entry_share_status']               = 'Allow users to share review';
$_['entry_product_page_limit']         = 'Limit reviews per page (product page)';
$_['entry_pros_cons_limit']            = 'Limit pros/cons can add by users';
$_['entry_pros_cons_character_limit']  = 'Character limit for the fields pros/cons';
$_['entry_captcha']                    = 'Logged customers do not have to fill out the field CAPTCHA';
$_['entry_seo_keyword']                = 'SEO keyword for page with all reviews';
$_['entry_default_view']               = 'Default view';
$_['entry_review_title_status']        = 'Show Review title field';
$_['entry_recommend_status']           = 'Show Recommend product field';
$_['entry_summary_status']             = 'Show reviews summary block';
$_['entry_point_status']               = 'Bonus points for writing reviews';
$_['entry_reward_point']               = 'How much to add?';
$_['entry_description_point']          = 'Description added points';
$_['entry_multistore_status']          = 'Separate reviews between stores';
$_['entry_language_status']            = 'Multilingual reviews';
$_['entry_image_thumb_width']          = 'Width thumb';
$_['entry_image_thumb_height']         = 'Height thumb';
$_['entry_image_popup_width']          = 'Width popup';
$_['entry_image_popup_height']         = 'Height popup';
$_['entry_purchase_status']            = 'Only customers who bought the product can write a review';
$_['entry_limit_product_status']       = 'Limiting the number of reviews';

// Help
$_['help_notify_status']               = 'Enable it, if you want to send information about new reviews to the admin.';
$_['help_notification']                = 'Message sent to the admin.';
$_['help_rating_guest']                = 'Only logged customer can write review.';
$_['help_autoapprove_rating']          = 'Average rating the product to automatically show reviews (auto approve reviews must be enabled).';
$_['help_appearance_type']             = 'The size of stars to rating.';
$_['help_appearance_customer_rating']  = 'The appearance of stars to rating of customer.';
$_['help_sort_status']                 = 'Sort reviews on the product page.';
$_['help_comment_status']              = 'Comment shows only on product page.';
$_['help_colorbox_status']             = 'If the popup image does not work on all the reviews it can switch on this option to fix it.';
$_['help_pros_cons_character_limit']   = 'From - To.';
$_['help_seo_keyword']                 = 'Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_default_view']                = 'Default view for page with all reviews.';
$_['help_point_status']                = 'Only logged customer.';
$_['help_multistore_status']           = 'If you are using multistore this option allows you to show only the reviews in the store at which it was added.';
$_['help_language_status']             = 'Show reviews in current language.';
$_['help_purchase_status']             = 'Only logged customer.';
$_['help_limit_product_status']        = 'The customer can write review only once for each product. Only logged customer.';

// Tab
$_['tab_general']         = 'Setting';
$_['tab_css']             = 'Customisation (CSS)';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify module Advanced Product Reviews!';
$_['error_required']     = 'Warning: Please check the form carefully for errors!';
$_['error_keyword']      = 'SEO keyword already in use!';
?>