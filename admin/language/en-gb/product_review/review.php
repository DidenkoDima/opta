<?php
// Heading
$_['heading_title']         = 'Reviews ';

// Text
$_['text_success']          = 'Success: You have modified Reviews!';
$_['text_success_optimize'] = 'Success: Optimized tables!';
$_['text_success_image']    = 'Success: Review image has been removed!';
$_['text_delete']           = 'Are you sure you want to delete image?';
$_['text_image_manager']    = 'Image Manager';
$_['text_browse']           = 'Upload';
$_['text_upload']           = 'Your file was successfully uploaded!';
$_['text_product']          = 'Product';
$_['text_review_title']     = 'Review title';
$_['text_author']           = 'Author';
$_['text_rating']           = 'Rating';
$_['text_comment']          = 'Comment reply';
$_['text_store']            = 'Store';
$_['text_language']         = 'Language';
$_['text_recommend']        = 'Recommend product';
$_['text_pros']             = 'Pros';
$_['text_cons']             = 'Cons';
$_['text_image']            = 'Image';
$_['text_status']           = 'Status';
$_['text_text']             = 'Text';
$_['text_date_added']       = 'Date added';
$_['text_date_reply']       = 'Date comment';
$_['text_review']           = 'Review details';
$_['text_list']             = 'Review list';
$_['text_add']              = 'Add review';
$_['text_edit']             = 'Edit review';
$_['text_confirm']          = 'Are you sure?';
$_['text_loading']          = 'loading';

// Column
$_['column_product']        = 'Product';
$_['column_author']         = 'Author';
$_['column_rating']         = 'Rating';
$_['column_avg']            = 'AVG';
$_['column_pros']           = 'Pros';
$_['column_cons']           = 'Cons';
$_['column_vote_yes']       = 'Helpfull (YES)';
$_['column_vote_no']        = 'Helpfull (NO)';
$_['column_store']          = 'Store';
$_['column_status']         = 'Status';
$_['column_date_added']     = 'Date Added';
$_['column_action']         = 'Action';

// Entry
$_['entry_product']         = 'Product';
$_['entry_review_title']    = 'Review title';
$_['entry_author']          = 'Author';
$_['entry_rating']          = 'Rating';
$_['entry_average']         = 'Average rating';
$_['entry_date_added']      = 'Date added';
$_['entry_comment']         = 'Comment reply';
$_['entry_store']           = 'Store';
$_['entry_language']        = 'Language';
$_['entry_pros']            = 'Pros';
$_['entry_cons']            = 'Cons';
$_['entry_image']           = 'Image';
$_['entry_comment_image']   = 'Images for a comment';
$_['entry_recommend']       = 'Recommend product';
$_['entry_status']          = 'Status';
$_['entry_text']            = 'Text';
$_['entry_good']            = 'Good';
$_['entry_bad']             = 'Bad';

// Help
$_['help_product']          = '(Autocomplete)';
$_['help_image']            = 'Up to %s images.';

// Button
$_['button_view']           = 'View';
$_['button_add']            = 'Add';

// Error
$_['error_permission']      = 'Warning: You do not have permission to modify Reviews!';
$_['error_product']         = 'Product required!';
$_['error_product_store']   = 'This product is not assigned to any store!';
$_['error_author']          = 'Author must be between 3 and 64 characters!';
$_['error_text']            = 'Review Text must be at least 1 character!';
$_['error_rating']          = 'Review rating required!';
$_['error_loading']         = 'Error retrieving data!';
$_['error_select']          = 'A first select any product!';
$_['error_review_id']       = 'Review ID is required!';
$_['error_review_image_id'] = 'Image is not exists!';
$_['error_filename']        = 'Filename must be between 3 and 64 characters!';
$_['error_upload']          = 'Upload required!';
$_['error_filetype']        = 'Invalid file type!';
$_['error_empty_rating']    = 'Go to <a href="%s">Manage ratings</a> and create rating list!';
?>
