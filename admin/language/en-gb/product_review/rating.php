<?php
// Heading
$_['heading_title']     = 'Manage ratings';

// Text
$_['text_success']      = 'Success: You have modified Ratings!';
$_['text_default']      = 'Default';
$_['text_list']         = 'Rating list';
$_['text_add']          = 'Add rating';
$_['text_edit']         = 'Edit rating';
$_['text_confirm']      = 'Are you sure?';

// Column
$_['column_name']       = 'Name';
$_['column_store']      = 'Store';
$_['column_sort_order'] = 'Sort order';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name'] 	    = 'Name';
$_['entry_store']       = 'Store';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_status']      = 'Status';

// Button
$_['button_add'] 	    = 'Add';

// Error
$_['error_name']        = 'Rating name must be between 3 and 64 characters!';
$_['error_permission']  = 'Warning: You do not have permission to modify Ratings!';
?>