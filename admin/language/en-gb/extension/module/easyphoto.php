<?php
// Heading
$_['heading_title']    = 'Easy Image Manager';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Easy Image Manager install success!';
$_['text_edit']        = 'Edit';
$_['text_manual']      = 'Setting is not required!';

// Entry
$_['entry_direct']     		= 'Images directory';
$_['easyphoto_direct_help'] = 'Directory to upload';
$_['entry_status']     		= 'Status';
$_['entry_prefix']     		= 'Photo prefix (dmYh)';
$_['entry_separator']     	= 'Q-ty elements in folder';

// Error
$_['error_permission'] = 'Permission error!';