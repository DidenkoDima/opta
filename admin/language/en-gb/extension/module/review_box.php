<?php
// Heading
$_['heading_title']       = 'Advanced Product Reviews - Box with reviews';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Advanced Product Reviews - Box!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_random']         = 'Random';
$_['text_latest']         = 'Latest';

// Entry
$_['entry_name']          = 'Module name';
$_['entry_type']          = 'Type box';
$_['entry_header']        = 'Header box';
$_['entry_limit']         = 'Limit';
$_['entry_all_review']    = 'Button (All reviews)';
$_['entry_layout']        = 'Layout';
$_['entry_position']      = 'Position';
$_['entry_status']        = 'Status';
$_['entry_sort_order']    = 'Sort Order';

// Button
$_['button_add_module']   = 'Add module';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify module Advanced Product Reviews - Box!';
$_['error_name']         = 'Module name must be between 3 and 64 characters!';
$_['error_header']       = 'Header title must be minimum 2 characters!';
?>