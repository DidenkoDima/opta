<?php

// Heading
$_['heading_title']             = 'Менеджер изображений';

// Text
$_['text_directory']            = 'Успешно: Папку создано!';
$_['text_delete']               = 'Успешно: Файл или папка были удалены!';
$_['text_rename']               = 'Успешно: Файл или папка были переименованы!';
$_['text_cropped']              = 'Успешно: Обрезанная картинка была сохранена!';
$_['text_details']              = 'Детали';
$_['text_library_folder']       = 'Изображения';
$_['text_details_none']         = 'Выберите файл, чтобы смотреть детали.';
$_['text_confirm']              = 'Вы действительно хотите %s';
$_['text_help']                 = 'Помощь';
$_['text_help_desc']            = 'Менеджер изображений';
$_['text_full_doc']             = '';

// Entry
$_['entry_search']              = 'Поиск..';
$_['entry_image_editor']        = 'Редактор изображений';
$_['entry_image_editor_desc_1'] = 'Выберите изображение из галереи и нажмите кнопку %s';
$_['entry_image_editor_desc_2'] = 'Или нажмите %s чтобы загрузить с компьютера';

// Tabs
$_['tab_gallery']               = 'Менеджер изображений';
$_['tab_editor']                = 'Редактор изображений';

// Details
$_['details_name']              = 'Название';
$_['details_size']              = 'Размер';
$_['details_location']          = 'Расположение';
$_['details_type']              = 'Тип';

// Editor
$_['editor_width']              = 'Ширина';
$_['editor_height']             = 'Высота';
$_['editor_drag']               = 'Перетянуть изображение';
$_['editor_crop']               = 'Обрезать изображение';
$_['editor_flip_horizontal']    = 'Развернуть по горизонтали';
$_['editor_flip_vertical']      = 'Развернуть по вертикали';
$_['editor_rotate_left']        = 'Повернуть влево';
$_['editor_rotate_right']       = 'Повернуть вправо';
$_['editor_zoom_in']            = 'Увеличить';
$_['editor_zoom_out']           = 'Уменьшить';
$_['editor_save_crop']          = 'Сохранить';
$_['editor_cancel_crop']        = 'Отменить';
$_['editor_reset_crop']         = 'Сбросить';
$_['editor_upload_new_image']   = 'Загрузить новое изображение';

// Status
$_['status_loading']            = 'Загрузка...';
$_['status_error']              = 'Ошибка';
$_['status_success']            = 'Успешно!';

// Buttons
$_['button_refresh']            = 'Обновить';
$_['button_folder']             = 'Новая папка';
$_['button_delete']             = 'Удалить';
$_['button_select']             = 'Выбрать';
$_['button_rename']             = 'Переименовать';
$_['button_crop']               = 'Обрезать';
$_['button_search']             = 'Поиск';
$_['button_upload']             = 'Загрузить';
$_['button_cancel']             = 'Отменить';
$_['button_cancel_selection']   = 'Отменить выбранное';
$_['button_ok']                 = 'OK';
$_['button_remote_upload']      = 'Удаленная загрузка';

// Confirmations
$_['remote_upload_title']       = 'Удаленная загрузка';
$_['remote_upload_desc']        = 'Вставьте ссылку на изображение для загрузки';
$_['remote_upload_placeholder'] = 'Ссылка';

$_['delete_title']              = 'Удалить изображение';
$_['delete_desc_singular']      = 'Вы уверены, что хотите удалить выбранный элемент? Возможности восстановить его - не будет.';
$_['delete_desc_plural']        = 'Вы уверены, что хотите удалить выбранные элементы? Возможности восстановить их - не будет.';

$_['folder_title']              = 'Создать папку';
$_['folder_desc']               = 'Введите название новой папки';
$_['folder_placeholder']        = 'Название';

$_['rename_title']              = 'Переименовать файл/папку';
$_['rename_desc']               = 'Изменение имени выбранного файла или папки';
$_['rename_placeholder']        = 'Название';

$_['savecrop_title']            = 'Сохранить изображение';
$_['savecrop_desc']             = 'Введите название нового изображения';
$_['savecrop_placeholder']      = 'Название';

// Error
$_['error_permission']          = 'Внимание: Доступ запрещен!';
$_['error_filename']            = 'Внимание: Имя файла должно быть от 3 до 255!';
$_['error_folder']              = 'Внимание: Имя папки должно быть от 3 до 255!';
$_['error_exists']              = 'Внимание: Файл или папка с таким именем уже существует!';
$_['error_directory']           = 'Внимание: Папки не существует!';
$_['error_filetype']            = 'Внимание: Неправильный тип файла!';
$_['error_upload']              = 'Внимание: Не удалось загрузить файл по неизвестной причине!';
$_['error_delete']              = 'Внимание: Вы не можете удалить эту папку!';
$_['error_url']                 = 'Внимание: Недопустимый URL изображения!';
$_['error_rename']              = 'Внимание: Введенное имя недействительно!';
$_['error_no_selection']        = 'Внимание: Нет выбранных файлов или папок!';
$_['error_no_moved']            = 'Внимание: Файлы или папки не были перемещены!';
$_['error_upload_blob']         = 'Внимание: Ваш браузер не поддерживает загрузку через URL! Обновите или измените браузер!';
$_['error_crop']                = 'Внимание: Невозможно сохранить обрезанное изображение!';

// Helpers
$_['helper_selecting_singular'] = 'Выбрано %s элемент';
$_['helper_selecting_plural']   = 'Выбрано %s элементов';
$_['helper_uploading_singular'] = 'Файл загружен...';
$_['helper_uploading_plural']   = 'Файлы загружены...';
$_['helper_uploaded_singular']  = 'Успешно: Ваш файл загружен!';
$_['helper_uploaded_plural']    = 'Успешно: Ваши файлы загружены!';
$_['helper_moved_singular']     = 'Успешно: Ваш файл или папка были перемещены!';
$_['helper_moved_plural']       = 'Успешно: Ваши файлы или папки были перемещены!';
$_['helper_creating_folder']    = 'Создание папки...';
$_['helper_deleting']           = 'Удаление...';
$_['helper_renaming']           = 'Переименование...';
$_['helper_moving']             = 'Перемещение...';