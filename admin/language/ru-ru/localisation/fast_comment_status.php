<?php
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['heading_title'] = 'Шаблоны Быстрых комментариев к истории(статусу) заказа';
$_['text_comment'] = 'Содержмое комментария';
$_['text_comment_help'] = 'Вы можете использовать макросы <br>{num} - номер заказа<br> {sum} - сумма заказа<br>{customer} - покупатель';
$_['text_name'] = 'Название шаблона';
$_['entry_name'] = 'Название шаблона';
$_['text_success'] = 'Настройки модуля обновлены';
$_['error_name'] = 'Имя шаблона должно юыть заполненно';
$_['heading_edit_template'] = 'редактирование шаблона быстрых комментаоиев';
$_['heading_add_template'] = 'Добавление шаблона быстрых комментариев';
$_['column_name'] = 'Название шаблона';
$_['column_action'] = 'Действия';
$_['text_no_results'] = 'Список пуст';
$_['text_list'] = 'Список шаблонов';
$_['text_edit'] = 'Редактирование';
$_['text_delete'] = 'Удалить';
$_['button_add'] = 'Добавить шаблон';
