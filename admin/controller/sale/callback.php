<?php

class ControllerSaleCallback extends Controller
{
	private $error = array();

	public function index()
	{
		$this->load->language("sale/callback");
		$this->load->model("sale/callback");
        
		$this->document->addScript("view/stylesheet/callbackpro/jscolor/jscolor.js");
		$this->document->addStyle("view/stylesheet/callbackpro/callback.css");
        
		$this->document->setTitle($this->language->get("heading_title_callback"));
        
		$time_start = microtime(true);
		// require_once ("callback_license/license_callback.php");
		// $shield = new Aleksey();
		// $shield->aleksey_5 = "./";
		// $shield->aleksey_24 = "callback.php";
		// $shield->aleksey_6 = "http://validator.waterfilter.in.ua/api.php";
		// $query_key = $this->db->query("SELECT `license_key` FROM " . DB_PREFIX . "key_callback where `key`='local_key'");
		// $query_row = $query_key->row;
		// $lkey = $query_row["license_key"];
		// if ($shield->aleksey_18 && $shield->aleksey_39() && $shield->isWindows() && !file_exists("$shield->aleksey_5$shield->aleksey_24")) {
		// 	$shield->aleksey_20 = true;
		// }
		// else if (empty($lkey)) {
		// 	$url = "";
		// 	$this->response->redirect($this->url->link("sale/callback/activation", "token=" . $this->session->data["token"] . $url, "SSL"));
		// }
		// $shield->aleksey_34 = $lkey;
		// $shield->aleksey_38();
		// if ($shield->aleksey_20) {
		$license = true;
		// }
		// else {
		// 	$url = "";
		// 	$this->response->redirect($this->url->link("sale/callback/activation", "token=" . $this->session->data["token"] . $url, "SSL"));
		// }
		$this->getList();
	}

	public function update()
	{
		$this->load->language("sale/callback");
		$this->document->setTitle($this->language->get("heading_title_callback"));
		$this->load->model("sale/callback");

		if ($this->request->server["REQUEST_METHOD"] == "POST") {
			$this->model_sale_callback->editCallback($this->request->get["callback_id"], $this->request->post);
			$this->session->data["success"] = $this->language->get("text_success");
			$url = "";

			if (isset($this->request->get["sort"])) {
				$url .= "&sort=" . $this->request->get["sort"];
			}

			if (isset($this->request->get["order"])) {
				$url .= "&order=" . $this->request->get["order"];
			}

			if (isset($this->request->get["page"])) {
				$url .= "&page=" . $this->request->get["page"];
			}

			$this->response->redirect($this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"));
		}

		$this->getForm();
	}

	public function update_batch()
	{
		$this->load->language("sale/callback");
		$this->document->setTitle($this->language->get("heading_title"));
		$this->load->model("sale/callback");

		if ($this->request->server["REQUEST_METHOD"] == "POST") {
			if (isset($this->request->post["selected"])) {
				foreach ($this->request->post["selected"] as $callback_id) {
					$this->model_sale_callback->editCallbacks($callback_id);
				}
			}

			$this->session->data["success"] = $this->language->get("text_success");
			$url = "";

			if (isset($this->request->get["sort"])) {
				$url .= "&sort=" . $this->request->get["sort"];
			}

			if (isset($this->request->get["order"])) {
				$url .= "&order=" . $this->request->get["order"];
			}

			if (isset($this->request->get["page"])) {
				$url .= "&page=" . $this->request->get["page"];
			}

			$this->response->redirect($this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"));
		}

		$this->getList();
	}

	public function delete()
	{
		$this->load->language("sale/callback");
		$this->document->setTitle($this->language->get("heading_title"));
		$this->load->model("sale/callback");

		if (isset($this->request->post["selected"]) && $this->validateDelete()) {
			foreach ($this->request->post["selected"] as $callback_id) {
				$this->model_sale_callback->deleteCallback($callback_id);
			}

			$this->session->data["success"] = $this->language->get("text_success");
			$url = "";

			if (isset($this->request->get["sort"])) {
				$url .= "&sort=" . $this->request->get["sort"];
			}

			if (isset($this->request->get["order"])) {
				$url .= "&order=" . $this->request->get["order"];
			}

			if (isset($this->request->get["page"])) {
				$url .= "&page=" . $this->request->get["page"];
			}

			$this->response->redirect($this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"));
		}

		$this->getList();
	}

	private function getlist()
	{
		if (isset($this->request->get["filter_callback_id"])) {
			$filter_callback_id = $this->request->get["filter_callback_id"];
		}
		else {
			$filter_callback_id = NULL;
		}

		if (isset($this->request->get["filter_fio"])) {
			$filter_fio = $this->request->get["filter_fio"];
		}
		else {
			$filter_fio = NULL;
		}

		if (isset($this->request->get["filter_phone"])) {
			$filter_phone = $this->request->get["filter_phone"];
		}
		else {
			$filter_phone = NULL;
		}

		if (isset($this->request->get["filter_email"])) {
			$filter_email = $this->request->get["filter_email"];
		}
		else {
			$filter_email = NULL;
		}

		if (isset($this->request->get["filter_date_added"])) {
			$filter_date_added = $this->request->get["filter_date_added"];
		}
		else {
			$filter_date_added = NULL;
		}

		if (isset($this->request->get["filter_status"])) {
			$filter_status = $this->request->get["filter_status"];
		}
		else {
			$filter_status = NULL;
		}

		if (isset($this->request->get["sort"])) {
			$sort = $this->request->get["sort"];
		}
		else {
			$sort = "call_id";
		}

		if (isset($this->request->get["order"])) {
			$order = $this->request->get["order"];
		}
		else {
			$order = "DESC";
		}

		if (isset($this->request->get["page"])) {
			$page = $this->request->get["page"];
		}
		else {
			$page = 1;
		}

		$url = "";
		$data["callback_setting"] = $this->url->link("sale/callback/settingcallback", "token=" . $this->session->data["token"] . $url, "SSL");

		if (isset($this->request->get["filter_callback_id"])) {
			$url .= "&filter_callback_id=" . $this->request->get["filter_callback_id"];
		}

		if (isset($this->request->get["filter_fio"])) {
			$url .= "&filter_fio=" . $this->request->get["filter_fio"];
		}

		if (isset($this->request->get["filter_phone"])) {
			$url .= "&filter_phone=" . $this->request->get["filter_phone"];
		}

		if (isset($this->request->get["filter_email"])) {
			$url .= "&filter_email=" . $this->request->get["filter_email"];
		}

		if (isset($this->request->get["filter_date_added"])) {
			$url .= "&filter_date_added=" . $this->request->get["filter_date_added"];
		}

		if (isset($this->request->get["filter_status"])) {
			$url .= "&filter_status=" . $this->request->get["filter_status"];
		}

		if (isset($this->request->get["sort"])) {
			$url .= "&sort=" . $this->request->get["sort"];
		}

		if (isset($this->request->get["order"])) {
			$url .= "&order=" . $this->request->get["order"];
		}

		if (isset($this->request->get["page"])) {
			$url .= "&page=" . $this->request->get["page"];
		}

		$data["breadcrumbs"] = array();
		$data["breadcrumbs"][] = array("text" => $this->language->get("text_home"), "href" => $this->url->link("common/dashboard", "token=" . $this->session->data["token"], "SSL"));
		$data["breadcrumbs"][] = array("text" => $this->language->get("heading_title"), "href" => $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"));
		$data["insert"] = $this->url->link("sale/callback/insert", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["delete"] = $this->url->link("sale/callback/delete", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["callbacks"] = array();
		$filter_data = array("filter_callback_id" => $filter_callback_id, "filter_fio" => $filter_fio, "filter_phone" => $filter_phone, "filter_email" => $filter_email, "filter_date_added" => $filter_date_added, "filter_status" => $filter_status, "sort" => $sort, "order" => $order, "start" => ($page - 1) * $this->config->get("config_limit_admin"), "limit" => $this->config->get("config_limit_admin"));
		$callbacks_total = $this->model_sale_callback->getTotalCallbacks($filter_data);
		$results = $this->model_sale_callback->getCallbacks($filter_data);

		foreach ($results as $result) {
			$action = $this->url->link("sale/callback/update", "token=" . $this->session->data["token"] . "&callback_id=" . $result["call_id"] . $url, "SSL");

			if ($result["status_id"] == "0") {
				$status = $this->language->get("status_wait");
			}
			else {
				$status = $this->language->get("status_done");
			}

			$data["callbacks"][] = array("callback_id" => $result["call_id"], "name" => $result["name"], "telephone" => $result["telephone"], "date_added" => date("j.m.Y - G:i", strtotime($result["date_added"])), "date_modified" => date("j.m.Y - G:i", strtotime($result["date_modified"])), "comment" => $result["comment"], "comment_buyer" => $result["comment_buyer"], "time_callback_on" => $result["time_callback_on"], "time_callback_off" => $result["time_callback_off"], "topic_callback_send" => $result["topic_callback_send"], "url_site" => $result["callback_url"], "email_buyer" => $result["email_buyer"], "username" => $result["username"], "action" => $action, "status" => $status, "selected" => isset($this->request->post["selected"]) && in_array($result["call_id"], $this->request->post["selected"]));
		}

		$this->language->load("user/user");
		$this->load->model("user/user");
		$results = $this->model_user_user->getUsers();

		foreach ($results as $result) {
			$data["users"][] = array("user_id" => $result["user_id"], "username" => $result["username"]);
		}

		$data["heading_title_callback"] = $this->language->get("heading_title_callback");
		$data["text_no_results"] = $this->language->get("text_no_results");
		$data["text_fio"] = $this->language->get("text_fio");
		$data["text_telephone"] = $this->language->get("text_telephone");
		$data["column_name"] = $this->language->get("column_name");
		$data["column_telephone"] = $this->language->get("column_telephone");
		$data["column_date_added"] = $this->language->get("column_date_added");
		$data["text_comment"] = $this->language->get("text_comment");
		$data["text_comment_buyer"] = $this->language->get("text_comment_buyer");
		$data["text_email_buyer"] = $this->language->get("text_email_buyer");
		$data["text_callback_url"] = $this->language->get("text_callback_url");
		$data["text_status"] = $this->language->get("text_status");
		$data["text_added"] = $this->language->get("text_added");
		$data["text_modified"] = $this->language->get("text_modified");
		$data["text_action"] = $this->language->get("text_action");
		$data["text_edit"] = $this->language->get("text_edit");
		$data["text_manager"] = $this->language->get("text_manager");
		$data["text_time_callback"] = $this->language->get("text_time_callback");
		$data["text_topic_callback"] = $this->language->get("text_topic_callback");
		$data["button_insert"] = $this->language->get("button_insert");
		$data["button_delete"] = $this->language->get("button_delete");
		$data["button_callback_setting"] = $this->language->get("button_callback_setting");
		$data["status_wait"] = $this->language->get("status_wait");
		$data["status_done"] = $this->language->get("status_done");
		$data["button_filter"] = $this->language->get("button_filter");
		$data["text_id"] = $this->language->get("text_id");
		$data["text_callback_url_info"] = $this->language->get("text_callback_url_info");
		$data["text_link"] = $this->language->get("text_link");
		$data["text_list"] = $this->language->get("text_list");
		$data["text_enabled"] = $this->language->get("text_enabled");
		$data["text_disabled"] = $this->language->get("text_disabled");
		$data["token"] = $this->session->data["token"];

		if (isset($this->error["warning"])) {
			$data["error_warning"] = $this->error["warning"];
		}
		else {
			$data["error_warning"] = "";
		}

		if (isset($this->session->data["success"])) {
			$data["success"] = $this->session->data["success"];
			unset($this->session->data["success"]);
		}
		else {
			$data["success"] = "";
		}

		$url = "";

		if ($order == "ASC") {
			$url .= "&order=DESC";
		}
		else {
			$url .= "&order=ASC";
		}

		if (isset($this->request->get["page"])) {
			$url .= "&page=" . $this->request->get["page"];
		}

		if (isset($this->request->get["filter_callback_id"])) {
			$url .= "&filter_callback_id=" . $this->request->get["filter_callback_id"];
		}

		if (isset($this->request->get["filter_fio"])) {
			$url .= "&filter_fio=" . $this->request->get["filter_fio"];
		}

		if (isset($this->request->get["filter_phone"])) {
			$url .= "&filter_phone=" . $this->request->get["filter_phone"];
		}

		if (isset($this->request->get["filter_email"])) {
			$url .= "&filter_email=" . $this->request->get["filter_email"];
		}

		if (isset($this->request->get["filter_date_added"])) {
			$url .= "&filter_date_added=" . urlencode(html_entity_decode($this->request->get["filter_date_added"], ENT_QUOTES, "UTF-8"));
		}

		if (isset($this->request->get["filter_status"])) {
			$url .= "&filter_status=" . $this->request->get["filter_status"];
		}

		$data["sort_call_id"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . "&sort=call_id" . $url, "SSL");
		$data["sort_name"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . "&sort=name" . $url, "SSL");
		$data["sort_telephone"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . "&sort=telephone" . $url, "SSL");
		$data["sort_username"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . "&sort=username" . $url, "SSL");
		$url = "";

		if (isset($this->request->get["sort"])) {
			$url .= "&sort=" . $this->request->get["sort"];
		}

		if (isset($this->request->get["order"])) {
			$url .= "&order=" . $this->request->get["order"];
		}

		if (isset($this->request->get["filter_callback_id"])) {
			$url .= "&filter_callback_id=" . $this->request->get["filter_callback_id"];
		}

		if (isset($this->request->get["filter_fio"])) {
			$url .= "&filter_fio=" . $this->request->get["filter_fio"];
		}

		if (isset($this->request->get["filter_phone"])) {
			$url .= "&filter_phone=" . $this->request->get["filter_phone"];
		}

		if (isset($this->request->get["filter_email"])) {
			$url .= "&filter_email=" . $this->request->get["filter_email"];
		}

		if (isset($this->request->get["filter_date_added"])) {
			$url .= "&filter_date_added=" . urlencode(html_entity_decode($this->request->get["filter_date_added"], ENT_QUOTES, "UTF-8"));
		}

		if (isset($this->request->get["filter_status"])) {
			$url .= "&filter_status=" . $this->request->get["filter_status"];
		}

		$pagination = new Pagination();
		$pagination->total = $callbacks_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get("config_limit_admin");
		$pagination->text = $this->language->get("text_pagination");
		$pagination->url = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url . "&page={page}", "SSL");
		$data["pagination"] = $pagination->render();
		$data["results"] = sprintf($this->language->get("text_pagination"), $callbacks_total ? (($page - 1) * $this->config->get("config_limit_admin")) + 1 : 0, ($callbacks_total - $this->config->get("config_limit_admin")) < (($page - 1) * $this->config->get("config_limit_admin")) ? $callbacks_total : (($page - 1) * $this->config->get("config_limit_admin")) + $this->config->get("config_limit_admin"), $callbacks_total, ceil($callbacks_total / $this->config->get("config_limit_admin")));
		$data["sort"] = $sort;
		$data["order"] = $order;
		$data["filter_callback_id"] = $filter_callback_id;
		$data["filter_fio"] = $filter_fio;
		$data["filter_phone"] = $filter_phone;
		$data["filter_email"] = $filter_email;
		$data["filter_date_added"] = $filter_date_added;
		$data["filter_status"] = $filter_status;
		$data["update"] = $this->url->link("sale/callback/update_batch", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["header"] = $this->load->controller("common/header");
		$data["column_left"] = $this->load->controller("common/column_left");
		$data["footer"] = $this->load->controller("common/footer");
		$this->response->setOutput($this->load->view("sale/callback_list.tpl", $data));
	}

	private function getform()
	{
		$this->load->language("sale/callback");
		$data["heading_title"] = $this->language->get("heading_title");
		$data["button_save"] = $this->language->get("button_save");
		$data["button_cancel"] = $this->language->get("button_cancel");
		$data["text_edit_callback"] = $this->language->get("text_edit_callback");
		$data["text_id"] = $this->language->get("text_id");
		$data["text_name"] = $this->language->get("text_name");
		$data["text_telephone"] = $this->language->get("text_telephone");
		$data["text_comment"] = $this->language->get("text_comment");
		$data["text_comment_buyer"] = $this->language->get("text_comment_buyer");
		$data["text_email_buyer"] = $this->language->get("text_email_buyer");
		$data["text_status"] = $this->language->get("text_status");
		$data["text_added"] = $this->language->get("text_added");
		$data["text_modified"] = $this->language->get("text_modified");
		$data["text_manager_form"] = $this->language->get("text_manager_form");
		$data["status_wait"] = $this->language->get("status_wait");
		$data["status_done"] = $this->language->get("status_done");
		$data["heading_title_callback"] = $this->language->get("heading_title_callback");
		$data["breadcrumbs"] = array();
		$data["breadcrumbs"][] = array("text" => $this->language->get("text_home"), "href" => $this->url->link("common/home", "token=" . $this->session->data["token"], "SSL"), "separator" => false);
		$url = "";
		$data["breadcrumbs"][] = array("text" => $this->language->get("heading_title"), "href" => $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"), "separator" => " :: ");
		$data["action"] = $this->url->link("sale/callback/update", "token=" . $this->session->data["token"] . "&callback_id=" . $this->request->get["callback_id"] . $url, "SSL");
		$data["cancel"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["token"] = $this->session->data["token"];

		if (isset($this->request->get["callback_id"]) && ($this->request->server["REQUEST_METHOD"] != "POST")) {
			$callback_info = $this->model_sale_callback->getСallback($this->request->get["callback_id"]);
		}

		$this->language->load("user/user");
		$this->load->model("user/user");
		$this->language->load("common/header");
		$data["logged"] = $this->user->getUserName();
		$results = $this->model_user_user->getUsers();

		foreach ($results as $result) {
			$data["users"][] = array("user_id" => $result["user_id"], "username" => $result["username"]);
		}

		if (isset($this->request->post["name"])) {
			$data["name"] = $this->request->post["name"];
		}
		else if (isset($callback_info)) {
			$data["name"] = $callback_info["name"];
		}

		if (isset($this->request->post["telephone"])) {
			$data["telephone"] = $this->request->post["telephone"];
		}
		else if (isset($callback_info)) {
			$data["telephone"] = $callback_info["telephone"];
		}

		if (isset($this->request->post["comment"])) {
			$data["comment"] = $this->request->post["comment"];
		}
		else if (isset($callback_info)) {
			$data["comment"] = $callback_info["comment"];
		}

		if (isset($this->request->post["username"])) {
			$data["username"] = $this->request->post["username"];
		}
		else if (isset($callback_info)) {
			$data["username"] = $callback_info["username"];
		}

		if (isset($this->request->post["email_buyer"])) {
			$data["email_buyer"] = $this->request->post["email_buyer"];
		}
		else if (isset($callback_info)) {
			$data["email_buyer"] = $callback_info["email_buyer"];
		}

		if (isset($this->request->post["status_id"])) {
			$data["status_id"] = $this->request->post["status_id"];
		}
		else if (isset($callback_info)) {
			$data["status_id"] = $callback_info["status_id"];
		}

		$data["username"] = $callback_info["username"];
		$data["comment_buyer"] = $callback_info["comment_buyer"];
		$data["callback_id"] = $callback_info["call_id"];
		$data["date_added"] = $callback_info["date_added"];
		$data["date_modified"] = $callback_info["date_modified"];
		$data["header"] = $this->load->controller("common/header");
		$data["column_left"] = $this->load->controller("common/column_left");
		$data["footer"] = $this->load->controller("common/footer");
		$this->response->setOutput($this->load->view("sale/callback_form.tpl", $data));
	}

	public function settingcallback()
	{
		$this->load->model("sale/callback");
		$this->load->language("sale/callback");
        
		$this->document->setTitle($this->language->get("heading_title_callback"));
        
		$this->document->addScript("view/stylesheet/callbackpro/jscolor/jscolor.js");
		$this->document->addScript("view/javascript/summernote/summernote.js");
		$this->document->addScript("view/javascript/summernote/opencart.js");
		$this->document->addStyle("view/javascript/summernote/summernote.css");
		$this->document->addStyle("view/stylesheet/callbackpro/callback.css");
        
		$data["heading_title"] = $this->language->get("heading_title");
		$data["button_save"] = $this->language->get("button_save");
		$data["button_cancel"] = $this->language->get("button_cancel");
		$data["breadcrumbs"] = array();
		$data["breadcrumbs"][] = array("text" => $this->language->get("text_home"), "href" => $this->url->link("common/home", "token=" . $this->session->data["token"], "SSL"), "separator" => false);
		$url = "";
		$data["breadcrumbs"][] = array("text" => $this->language->get("heading_title"), "href" => $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"), "separator" => " :: ");
		$this->load->model("localisation/language");
		$data["languages"] = $this->model_localisation_language->getLanguages();
		$data["text_yes"] = $this->language->get("text_yes");
		$data["text_no"] = $this->language->get("text_no");
		$data["entry_background_callback_header"] = $this->language->get("entry_background_callback_header");
		$data["entry_text_color_callback_header"] = $this->language->get("entry_text_color_callback_header");
		$data["entry_icon_callback_header"] = $this->language->get("entry_icon_callback_header");
		$data["entry_border_color_button_callback"] = $this->language->get("entry_border_color_button_callback");
		$data["entry_border_color_button_callback_hover"] = $this->language->get("entry_border_color_button_callback_hover");
		$data["entry_background_callback_footer"] = $this->language->get("entry_background_callback_footer");
		$data["entry_boxshadow_img_callback"] = $this->language->get("entry_boxshadow_img_callback");
		$data["entry_background_callback_center"] = $this->language->get("entry_background_callback_center");
		$data["text_select_icon"] = $this->language->get("text_select_icon");
		$data["text_on_off_date_time"] = $this->language->get("text_on_off_date_time");
		$data["entry_position_callback"] = $this->language->get("entry_position_callback");
		$data["text_position_top_left"] = $this->language->get("text_position_top_left");
		$data["text_position_center_left"] = $this->language->get("text_position_center_left");
		$data["text_position_bottom_left"] = $this->language->get("text_position_bottom_left");
		$data["text_position_top_right"] = $this->language->get("text_position_top_right");
		$data["text_position_center_right"] = $this->language->get("text_position_center_right");
		$data["text_position_bottom_right"] = $this->language->get("text_position_bottom_right");
		$data["entry_title_callback_sendthis"] = $this->language->get("entry_title_callback_sendthis");
		$data["entry_instruction_icon_change"] = $this->language->get("entry_instruction_icon_change");
		$data["entry_icon_class"] = $this->language->get("entry_icon_class");
		$data["entry_title_link"] = $this->language->get("entry_title_link");
		$data["entry_icon_link_color"] = $this->language->get("entry_icon_link_color");
		$data["entry_background_tooltip_callback"] = $this->language->get("entry_background_tooltip_callback");
		$data["entry_color_tooltip_callback"] = $this->language->get("entry_color_tooltip_callback");
		$data["entry_border_tooltip_callback"] = $this->language->get("entry_border_tooltip_callback");
		$data["entry_title_mob"] = $this->language->get("entry_title_mob");
		$data["entry_mob"] = $this->language->get("entry_mob");
		$data["text_design_theme_callback_center"] = $this->language->get("text_design_theme_callback_center");
		$data["text_design_theme_callback_lr"] = $this->language->get("text_design_theme_callback_lr");
		$data["text_right_callback_position_fixed"] = $this->language->get("text_right_callback_position_fixed");
		$data["entry_animate_btn"] = $this->language->get("entry_animate_btn");
		$data["entry_background_phone1"] = $this->language->get("entry_background_phone1");
		$data["entry_background_phone3"] = $this->language->get("entry_background_phone3");
		$data["entry_email"] = $this->language->get("entry_email");
		$data["entry_color_email"] = $this->language->get("entry_color_email");
		$data["entry_skype"] = $this->language->get("entry_skype");
		$data["time_on_status_skype"] = $this->language->get("time_on_status_skype");
		$data["time_off_status_skype"] = $this->language->get("time_off_status_skype");
		$data["on_off_status_skype"] = $this->language->get("on_off_status_skype");
		$data["entry_color_skype"] = $this->language->get("entry_color_skype");
		$data["entry_title_schedule"] = $this->language->get("entry_title_schedule");
		$data["entry_color_clock"] = $this->language->get("entry_color_clock");
		$data["entry_daily"] = $this->language->get("entry_daily");
		$data["entry_weekend"] = $this->language->get("entry_weekend");
		$data["entry_background_callback"] = $this->language->get("entry_background_callback");
		$data["entry_background_button_callback"] = $this->language->get("entry_background_button_callback");
		$data["entry_background_button_callback_hover"] = $this->language->get("entry_background_button_callback_hover");
		$data["entry_phone_number_send_sms"] = $this->language->get("entry_phone_number_send_sms");
		$data["entry_login_send_sms"] = $this->language->get("entry_login_send_sms");
		$data["entry_pass_send_sms"] = $this->language->get("entry_pass_send_sms");
		$data["register_site"] = $this->language->get("register_site");
		$data["text_setting_module"] = $this->language->get("text_setting_module");
		$data["tab_fields_setting"] = $this->language->get("tab_fields_setting");
		$data["tab_general_setting"] = $this->language->get("tab_general_setting");
		$data["tab_design_setting"] = $this->language->get("tab_design_setting");
		$data["tab_sms_setting"] = $this->language->get("tab_sms_setting");
		$data["tab_email_setting"] = $this->language->get("tab_email_setting");
		$data["text_status_fields"] = $this->language->get("text_status_fields");
		$data["text_requared_fields"] = $this->language->get("text_requared_fields");
		$data["text_placeholder_fields"] = $this->language->get("text_placeholder_fields");
		$data["text_on_off_fields_firstname"] = $this->language->get("text_on_off_fields_firstname");
		$data["text_on_off_fields_phone"] = $this->language->get("text_on_off_fields_phone");
		$data["text_on_off_fields_comment"] = $this->language->get("text_on_off_fields_comment");
		$data["text_on_off_fields_email"] = $this->language->get("text_on_off_fields_email");
		$data["text_complete_quickorder"] = $this->language->get("text_complete_quickorder");
		$data["entry_mask_phone_number"] = $this->language->get("entry_mask_phone_number");
		$data["on_off_sms_callback"] = $this->language->get("on_off_sms_callback");
		$data["text_delete"] = $this->language->get("text_delete");
		$data["text_add"] = $this->language->get("text_add");
		$data["tab_contact_setting"] = $this->language->get("tab_contact_setting");
		$data["entry_social_block_title"] = $this->language->get("entry_social_block_title");
		$data["entry_any_text_bottom_before_button"] = $this->language->get("entry_any_text_bottom_before_button");
		$data["form_latter_from_me_callback"] = $this->language->get("form_latter_from_me_callback");
		$data["text_on_off_send_me_mail_callback"] = $this->language->get("text_on_off_send_me_mail_callback");
		$data["callback_subject"] = $this->language->get("callback_subject");
		$data["subject_text_variables"] = $this->language->get("subject_text_variables");
		$data["entry_you_email_callback"] = $this->language->get("entry_you_email_callback");
		$data["list_of_variables_entry"] = $this->language->get("list_of_variables_entry");
		$data["quickorder_description_me"] = $this->language->get("quickorder_description_me");
		$data["text_select_design_theme_callback"] = $this->language->get("text_select_design_theme_callback");
		$data["entry_background_callback_hover"] = $this->language->get("entry_background_callback_hover");
		$data["text_on_off_contact_right"] = $this->language->get("text_on_off_contact_right");
		$data["text_theme2_right"] = $this->language->get("text_theme2_right");
		$data["text_theme2_left"] = $this->language->get("text_theme2_left");
		$data["text_theme_callback"] = $this->language->get("text_theme_callback");
		$data["entry_social_callback"] = $this->language->get("entry_social_callback");
		$data["add"] = $this->language->get("add");
		$data["button_image_add"] = $this->language->get("add");
		$data["button_remove"] = $this->language->get("button_remove");
		$this->load->model("tool/image");
		$data["placeholder"] = $this->model_tool_image->resize("no_image.png", 100, 100);
		$data["entry_img_left_callback"] = $this->language->get("entry_img_left_callback");

		if (isset($this->request->post["callbackpro"])) {
			$data["callbackpro"] = $this->request->post["callbackpro"];
		}
		else {
			$data["callbackpro"] = $this->config->get("callbackpro");
		}

		if (isset($this->request->post["callbackpro"]["main_image_callback"])) {
			$data["main_image_callback"] = $this->request->post["callbackpro"]["main_image_callback"];
		}
		else {
			$data["main_image_callback"] = $data["callbackpro"]["main_image_callback"];
		}

		if (isset($this->request->post["callbackpro"]["main_image_callback"]) && is_file(DIR_IMAGE . $this->request->post["callbackpro"]["main_image_callback"])) {
			$data["main_thumb_callback"] = $this->model_tool_image->resize($this->request->post["callbackpro"]["main_image_callback"], 100, 100);
		}
		else {
			if ($data["callbackpro"]["main_image_callback"] && is_file(DIR_IMAGE . $data["callbackpro"]["main_image_callback"])) {
				$data["main_thumb_callback"] = $this->model_tool_image->resize($data["callbackpro"]["main_image_callback"], 100, 100);
			}
			else {
				$data["main_thumb_callback"] = $this->model_tool_image->resize("no_image.png", 100, 100);
			}
		}

		if (!empty($data["callbackpro"]["config_phones"])) {
			$data["config_phones"] = $data["callbackpro"]["config_phones"];
		}
		else {
			$data["config_phones"] = array();
		}

		$data["no_image_phone"] = $this->model_tool_image->resize("no_image.png", 100, 100);
		$header_phones = $data["config_phones"];

		if (!empty($header_phones)) {
			$data["config_phones_array"] = array();

			foreach ($header_phones as $phone_header) {
				if (isset($phone_header["image"]) && is_file(DIR_IMAGE . $phone_header["image"])) {
					$data["icon_phone"] = $this->model_tool_image->resize($phone_header["image"], 100, 100);
				}
				else {
					$data["icon_phone"] = $this->model_tool_image->resize("no_image.png", 100, 100);
				}

				$data["config_phones_array"][] = array("image" => $phone_header["image"], "icon_phone" => $data["icon_phone"], "phone" => $phone_header["phone"]);
			}
		}

		if (!empty($data["callbackpro"]["social_icon"])) {
			$data["social_data"] = $data["callbackpro"]["social_icon"];
		}
		else {
			$data["social_data"] = array();
		}

		$data["social_icons"] = array();

		foreach ($data["social_data"] as $result) {
			if (is_file(DIR_IMAGE . $result["image"])) {
				$image = $result["image"];
				$thumb = $result["image"];
			}
			else {
				$image = "";
				$thumb = "no_image.png";
			}

			$data["social_icons"][] = array("name" => $result["name"], "image" => $result["image"], "thumb" => $this->model_tool_image->resize($image, 100, 100));
		}

		if (!empty($data["callbackpro"]["call_topic"])) {
			$data["call_topic_data"] = $data["callbackpro"]["call_topic"];
		}
		else {
			$data["call_topic_data"] = array();
		}

		$data["token"] = $this->session->data["token"];
		$data["action"] = $this->url->link("sale/callback/settingcallback", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["cancel"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL");
		$this->load->model("setting/setting");

		if (($this->request->server["REQUEST_METHOD"] == "POST") && $this->validateForm()) {
			$this->model_setting_setting->editSetting("callbackpro", $this->request->post);

			if (isset($_POST["social"])) {
				foreach ($_POST["social"] as $data) {
					$this->model_sale_callback->changeSocial($data);
				}
			}

			if (isset($_POST["new_social"])) {
				foreach ($_POST["new_social"] as $data) {
					$this->model_sale_callback->addSocial($data);
				}
			}

			$this->generateCss($this->request->post);
			$this->session->data["success"] = $this->language->get("text_success");
			$this->response->redirect($this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"));
		}

		$data["token"] = $this->session->data["token"];

		if (isset($this->error["warning"])) {
			$data["error_warning"] = $this->error["warning"];
		}
		else {
			$data["error_warning"] = "";
		}

		$data["heading_title_callback"] = $this->language->get("heading_title_callback");
		$data["header"] = $this->load->controller("common/header");
		$data["column_left"] = $this->load->controller("common/column_left");
		$data["footer"] = $this->load->controller("common/footer");
		$this->response->setOutput($this->load->view("sale/callback_setting.tpl", $data));
	}

	public function activation()
	{
		$this->load->model("sale/callback");
		$this->load->language("sale/callback");
		$data["heading_title"] = $this->language->get("heading_title");
		$data["button_save"] = $this->language->get("button_save");
		$data["button_cancel"] = $this->language->get("button_cancel");
		$data["add_activation_key"] = $this->language->get("add_activation_key");
		$data["activated_btn"] = $this->language->get("activated_btn");
		$data["btn_deactivation"] = $this->language->get("btn_deactivation");
		$data["enter_deactivation_key"] = $this->language->get("enter_deactivation_key");
		$data["breadcrumbs"] = array();
		$data["breadcrumbs"][] = array("text" => $this->language->get("text_home"), "href" => $this->url->link("common/home", "token=" . $this->session->data["token"], "SSL"), "separator" => false);
		$url = "";
		$data["breadcrumbs"][] = array("text" => $this->language->get("heading_title"), "href" => $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"), "separator" => " :: ");
		$this->load->model("localisation/language");
		$data["languages"] = $this->model_localisation_language->getLanguages();
		$data["token"] = $this->session->data["token"];
		$data["action"] = $this->url->link("sale/callback/activation", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["cancel"] = $this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL");
		$data["heading_title_activation"] = $this->language->get("heading_title_activation");

		if (($this->request->server["REQUEST_METHOD"] == "POST") && $this->validateActivation($this->request->post["license_key"])) {
			$key = $this->request->post["license_key"];
			$this->session->data["success"] = $this->language->get("the_module_is_activated");
			$this->db->query("UPDATE " . DB_PREFIX . "key_callback SET `license_key`='' WHERE `key`='local_key'");
			$this->db->query("UPDATE " . DB_PREFIX . "key_callback SET `license_key`='" . $key . "' where `key`='local_key'");
			$this->response->redirect($this->url->link("sale/callback", "token=" . $this->session->data["token"] . $url, "SSL"));
		}

		$data["token"] = $this->session->data["token"];

		if (isset($this->error["warning"])) {
			$data["error_warning"] = $this->error["warning"];
		}
		else {
			$data["error_warning"] = "";
		}

		$data["heading_title_callback"] = $this->language->get("heading_title_callback");
		$data["header"] = $this->load->controller("common/header");
		$data["column_left"] = $this->load->controller("common/column_left");
		$data["footer"] = $this->load->controller("common/footer");
		$this->response->setOutput($this->load->view("sale/callback_activation.tpl", $data));
	}

	public function deactivation()
	{
		$json = array();
		$this->load->language("sale/callback");
		$data["key_success_deactivation"] = $this->language->get("key_success_deactivation");
		$data["this_key_is_not_present_enter_the_correct_key"] = $this->language->get("this_key_is_not_present_enter_the_correct_key");
		$data["key_error_deactivation"] = $this->language->get("key_error_deactivation");
		$data["enter_key_deactivation"] = $this->language->get("enter_key_deactivation");

		// if (isset($this->request->get["license_key_deactivation"]) && ($this->request->get["license_key_deactivation"] != "")) {
		// 	$json = array();
		// 	$key = $this->request->get["license_key_deactivation"];
		// 	$url = "http://validator.waterfilter.in.ua/admin/ajax/keystatus_deactivation.php?key=" . $key . "&status=3";
		// 	$ch = curl_init();
		// 	curl_setopt($ch, CURLOPT_URL, $url);
		// 	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		// 	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		// 	$result = curl_exec($ch);
		// 	curl_close($ch);
		// 	$json = array();

		// 	if ($result == "ok") {
		// 		$this->db->query("UPDATE " . DB_PREFIX . "key_callback SET `license_key`='' WHERE `key`='local_key'");
		// 		require_once ("callback_license/license_callback.php");
		// 		$shield = new Aleksey();
		// 		$shield->aleksey_5 = "./";
		// 		$shield->aleksey_24 = "callback.php";
		// 		$shield->aleksey_6 = "http://validator.waterfilter.in.ua/api.php";
		// 		$shield->clearLocalKey();
				$json["success"] = $data["key_success_deactivation"];
		// 	}
		// 	else if ($result == "false") {
		// 		$this->db->query("UPDATE " . DB_PREFIX . "key_callback SET `license_key`='' WHERE `key`='local_key'");
		// 		require_once ("callback_license/license_callback.php");
		// 		$shield = new Aleksey();
		// 		$shield->aleksey_5 = "./";
		// 		$shield->aleksey_24 = "callback.php";
		// 		$shield->aleksey_6 = "http://validator.waterfilter.in.ua/api.php";
		// 		$shield->clearLocalKey();
		// 		$json["error"] = $data["this_key_is_not_present_enter_the_correct_key"];
		// 	}
		// 	else {
		// 		$this->db->query("UPDATE " . DB_PREFIX . "key_callback SET `license_key`='' WHERE `key`='local_key'");
		// 		require_once ("callback_license/license_callback.php");
		// 		$shield = new Aleksey();
		// 		$shield->aleksey_5 = "./";
		// 		$shield->aleksey_24 = "callback.php";
		// 		$shield->aleksey_6 = "http://validator.waterfilter.in.ua/api.php";
		// 		$shield->clearLocalKey();
		// 		$json["error"] = $data["key_error_deactivation"];
		// 	}
		// }
		// else {
		// 	$this->db->query("UPDATE " . DB_PREFIX . "key_callback SET `license_key`='' WHERE `key`='local_key'");
		// 	require_once ("callback_license/license_callback.php");
		// 	$shield = new Aleksey();
		// 	$shield->aleksey_5 = "./";
		// 	$shield->aleksey_24 = "callback.php";
		// 	$shield->aleksey_6 = "http://validator.waterfilter.in.ua/api.php";
		// 	$shield->clearLocalKey();
		// 	$json["error"] = $data["enter_key_deactivation"];
		// }

		return $this->response->setOutput(json_encode($json));
	}

	private function validateactivation($key)
	{
		// $time_start = microtime(true);
		// require_once ("callback_license/license_callback.php");
		// $shield = new Aleksey();
		// $shield->aleksey_5 = "./";
		// $shield->aleksey_24 = "callback.php";
		// $shield->aleksey_6 = "http://validator.waterfilter.in.ua/api.php";
		// $shield->aleksey_34 = $key;
		// $shield->aleksey_38();

		// if ($shield->aleksey_20 == "") {
		// 	$this->error["warning"] = $shield->aleksey_40;
		// }

		return !$this->error;
	}

	protected function validate()
	{
		if (!$this->user->hasPermission("modify", "sale/order")) {
			$this->error["warning"] = $this->language->get("error_permission");
		}

		return !$this->error;
	}

	protected function validatedelete()
	{
		if (!$this->user->hasPermission("modify", "sale/order")) {
			$this->error["warning"] = $this->language->get("error_permission");
		}

		return !$this->error;
	}

	private function validateform()
	{
		if (!$this->user->hasPermission("modify", "sale/callback")) {
			$this->error["warning"] = $this->language->get("error_permission");
		}

		if (!$this->error) {
			return true;
		}

		return false;
	}

	public function deletesocial()
	{
		if ($this->request->server["REQUEST_METHOD"] == "POST") {
			$this->db->query("DELETE FROM " . DB_PREFIX . "social_table WHERE social_id = '" . (double) $this->request->post["social_id"] . "'");
		}
	}

	public function deletesubjectcall()
	{
		if ($this->request->server["REQUEST_METHOD"] == "POST") {
			$this->db->query("DELETE FROM " . DB_PREFIX . "call_topic WHERE id = '" . (double) $this->request->post["call_topic_id"] . "'");
		}
	}

	public function generatecss($data)
	{
		$status_animate_btn_1 = isset($data["callbackpro"]["status_animate_btn_1"]);
		$position_animate_btn_1 = $data["callbackpro"]["position_animate_btn_1"];
        
		$status_animate_btn_2 = isset($data["callbackpro"]["status_animate_btn_2"]);
		$position_animate_btn_2 = $data["callbackpro"]["position_animate_btn_2"];
        
		$status_animate_btn_3 = isset($data["callbackpro"]["status_animate_btn_3"]);
		$position_animate_btn_3 = $data["callbackpro"]["position_animate_btn_3"];
        
		$status_animate_btn_4 = isset($data["callbackpro"]["status_animate_btn_4"]);
		$position_animate_btn_4 = $data["callbackpro"]["position_animate_btn_4"];

		if (!isset($data["callbackpro"]["csscallback"])) {
			return;
		}

		$file = DIR_CATALOG . "view/theme/default/stylesheet/csscallback.css";
		$dataStr = "";

		foreach ($data['callbackpro']['csscallback'] as $item) {
			$name = $data['callbackpro']['csscallback'];

			switch ($name) {
			case 'background_callbackpro_header':
				$dataStr .= '#popup-callback .popup-heading {background:' . $item . ';}';
				break;
			
			case 'text_color_callbackpro_header':
				$dataStr .= '#popup-callback .popup-heading {color:' . $item . ';}';
				break;

			case 'background_callback_center':
				$dataStr .= '#popup-callback .popup-center{background:' . $item . ';}';
				break;

			case 'boxshadow_img_callback':
				$dataStr .= '#popup-callback .popup-center .circular {box-shadow:0 1px 5px 2px ' . $item . ';}';
				break;

			case 'background_contact_content':
				$dataStr .= '#popup-callback .popup-center #callbackpro_contacts {background:' . $item . ';}';
				$dataStr .= '#popup-callback .popup-center .contact-open:before, .contact-open:after{background:' . $item . ';}';
				$dataStr .= '#popup-callback .popup-center .contact-open{background:' . $item . ';}';
				break;

			case 'color_contact_content':
				$dataStr .= '#popup-callback .popup-center .contact-open a,#popup-callback .email div,#popup-callback .schedule .config_daily, #popup-callback .schedule .config_weekend,#popup-callback .skype a,#popup-callback .title-social,#popup-callback .telephone div a {color:' . $item . ';}';
				break;

			case 'config_background_callback':
				$dataStr .= '#tcb-call .tcb-phone { background:' . $item . ';}';
				$dataStr .= '#tcb-call .tcb-layout1 { border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call .tcb-layout2 {border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call .tcb-layout3 {border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb_call_1 .tcb_phone_1 { background:' . $item . ';}';
				$dataStr .= '#tcb_call_1 .tcb_layout_1 {border-color:' . $item . ' transparent;}';
				$dataStr .= '#tcb_call_1 .tcb_layout_2 {border-color:' . $item . ';}';
				$dataStr .= '#callback {background:' . $item . ';}';
				$dataStr .= '.animate_btn_3 .ab3-back-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3 .ab3-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3 .ab3-track {border: 2px solid ' . $item . ';}';
				$dataStr .= '.animate_btn_4 .ab4-img-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4 .ab4-circle-fill {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4 .ab4-circlephone {border:2px solid ' . $item . ';}';
				break;

			case 'config_background_callback_hover':
				$dataStr .= '#tcb-call:hover .tcb-phone{background:' . $item . '}';
				$dataStr .= '#tcb-call:hover .tcb-layout1{border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call:hover .tcb-layout2{border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call:hover .tcb-layout3{border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb_call_1:hover .tcb_phone_1{background:' . $item . ';}';
				$dataStr .= '#tcb_call_1:hover .tcb_layout_1{border-color:' . $item . ' transparent;}';
				$dataStr .= '#tcb_call_1:hover .tcb_layout_2{border-color:' . $item . ';}';
				$dataStr .= '#callback:hover {background:' . $item . ';}';
				$dataStr .= '.animate_btn_3:hover .ab3-back-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3:hover .ab3-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3:hover .ab3-track {border: 2px solid ' . $item . ';}';
				$dataStr .= '.animate_btn_4:hover .ab4-img-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4:hover .ab4-circle-fill {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4:hover .ab4-circlephone {border:2px solid ' . $item . ';}';
				break;

			case 'config_background_button_callback':
				$dataStr .= '#popup-callback .btn-callback {background-color:' . $item . ';}';
				break;
			
			case 'config_background_button_callback_hover':
				$dataStr .= '#popup-callback .btn-callback:hover, #popup-callback .btn-callback:active, #popup-callback .btn-callback:focus {background-color:' . $item . ';}';
				break;

			case 'config_border_color_button_callback':
				$dataStr .= '#popup-callback .btn-callback {border-color:' . $item . ';}';
				break;

			case 'config_border_color_button_callback_hover':			
				$dataStr .= '#popup-callback .btn-callback:hover, .btn-callback:active, .btn-callback:focus { border-color:' . $item . ';}';
				break;
			
			case 'background_callback_footer':
				$dataStr .= '#popup-callback .popup-footer {background:' . $item . ';}';
				break;

			case 'text_color_callbackpro_header':
				$dataStr .= '#popup-callback .popup-heading {color:' . $item . ';}';
				break;
			case 'background_callback_center':
				$dataStr .= '#popup-callback .popup-center{background:' . $item . ';}';
				break;
			case 'boxshadow_img_callback':
				$dataStr .= '#popup-callback .popup-center .circular {box-shadow:0 1px 5px 2px ' . $item . ';}';
				break;
			case 'background_contact_content':
				$dataStr .= '#popup-callback .popup-center #callbackpro_contacts {background:' . $item . ';}';
				$dataStr .= '#popup-callback .popup-center .contact-open:before, .contact-open:after{background:' . $item . ';}';
				$dataStr .= '#popup-callback .popup-center .contact-open{background:' . $item . ';}';
				break;
			case 'color_contact_content':
				$dataStr .= '#popup-callback .popup-center .contact-open a,#popup-callback .email div,#popup-callback .schedule .config_daily, #popup-callback .schedule .config_weekend,#popup-callback .skype a,#popup-callback .title-social,#popup-callback .telephone div a {color:' . $item . ';}';
				break;
			case 'config_background_callback':
				$dataStr .= '#tcb-call .tcb-phone { background:' . $item . ';}';
				$dataStr .= '#tcb-call .tcb-layout1 { border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call .tcb-layout2 {border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call .tcb-layout3 {border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb_call_1 .tcb_phone_1 { background:' . $item . ';}';
				$dataStr .= '#tcb_call_1 .tcb_layout_1 {border-color:' . $item . ' transparent;}';
				$dataStr .= '#tcb_call_1 .tcb_layout_2 {border-color:' . $item . ';}';
				$dataStr .= '#callback {background:' . $item . ';}';
				$dataStr .= '.animate_btn_3 .ab3-back-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3 .ab3-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3 .ab3-track {border: 2px solid ' . $item . ';}';
				$dataStr .= '.animate_btn_4 .ab4-img-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4 .ab4-circle-fill {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4 .ab4-circlephone {border:2px solid ' . $item . ';}';
				break;
			case 'config_background_callback_hover':
				$dataStr .= '#tcb-call:hover .tcb-phone{background:' . $item . '}';
				$dataStr .= '#tcb-call:hover .tcb-layout1{border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call:hover .tcb-layout2{border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb-call:hover .tcb-layout3{border-color:' . $item . ' transparent transparent;}';
				$dataStr .= '#tcb_call_1:hover .tcb_phone_1{background:' . $item . ';}';
				$dataStr .= '#tcb_call_1:hover .tcb_layout_1{border-color:' . $item . ' transparent;}';
				$dataStr .= '#tcb_call_1:hover .tcb_layout_2{border-color:' . $item . ';}';
				$dataStr .= '#callback:hover {background:' . $item . ';}';
				$dataStr .= '.animate_btn_3:hover .ab3-back-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3:hover .ab3-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_3:hover .ab3-track {border: 2px solid ' . $item . ';}';
				$dataStr .= '.animate_btn_4:hover .ab4-img-circle {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4:hover .ab4-circle-fill {background-color:' . $item . ';}';
				$dataStr .= '.animate_btn_4:hover .ab4-circlephone {border:2px solid ' . $item . ';}';
				break;
			case 'config_background_button_callback':
				$dataStr .= '#popup-callback .btn-callback {background-color:' . $item . ';}';
				break;
			case 'config_background_button_callback_hover':
				$dataStr .= '#popup-callback .btn-callback:hover, #popup-callback .btn-callback:active, #popup-callback .btn-callback:focus {background-color:' . $item . ';}';
				break;
			case 'config_border_color_button_callback':
				$dataStr .= '#popup-callback .btn-callback {border-color:' . $item . ';}';
				break;
			case 'config_border_color_button_callback_hover':
				$dataStr .= '#popup-callback .btn-callback:hover, .btn-callback:active, .btn-callback:focus { border-color:' . $item . ';}';
				break;
			case 'background_callback_footer':
				$dataStr .= '#popup-callback .popup-footer {background:' . $item . ';}';
				break;

			}
		}

		if (isset($status_animate_btn_1)) {
			if ($position_animate_btn_1 == "1") {
				$dataStr .= "#tcb_call_1 {left:33px; bottom: 80% !important;}";
			}
			else if ($position_animate_btn_1 == "2") {
				$dataStr .= "#tcb_call_1 {left:33px; bottom: 45% !important;}";
			}
			else if ($position_animate_btn_1 == "3") {
				$dataStr .= "#tcb_call_1 {left:33px; bottom: 10% !important;}";
			}
			else if ($position_animate_btn_1 == "4") {
				$dataStr .= "#tcb_call_1 {bottom: 80% !important;}";
			}
			else if ($position_animate_btn_1 == "5") {
				$dataStr .= "#tcb_call_1 {bottom: 45% !important;}";
			}
			else if ($position_animate_btn_1 == "6") {
				$dataStr .= "#tcb_call_1 {bottom: 10% !important;}";
			}
		}

		if (isset($status_animate_btn_2)) {
			if ($position_animate_btn_2 == "1") {
				$dataStr .= "#tcb-call {left:33px; bottom: 80% !important;}";
			}
			else if ($position_animate_btn_2 == "2") {
				$dataStr .= "#tcb-call {left:33px; bottom: 45% !important;}";
			}
			else if ($position_animate_btn_2 == "3") {
				$dataStr .= "#tcb-call {left:33px; bottom: 10% !important;}";
			}
			else if ($position_animate_btn_2 == "4") {
				$dataStr .= "#tcb-call {bottom: 80% !important;}";
			}
			else if ($position_animate_btn_2 == "5") {
				$dataStr .= "#tcb-call {bottom: 45% !important;}";
			}
			else if ($position_animate_btn_2 == "6") {
				$dataStr .= "#tcb-call {bottom: 10% !important;}";
			}
		}

		if (isset($status_animate_btn_3)) {
			if ($position_animate_btn_3 == "1") {
				$dataStr .= "#animate_btn_3 {left:33px; bottom: 80% !important;}";
			}
			else if ($position_animate_btn_3 == "2") {
				$dataStr .= "#animate_btn_3 {left:33px; bottom: 45% !important;}";
			}
			else if ($position_animate_btn_3 == "3") {
				$dataStr .= "#animate_btn_3 {left:33px; bottom: 10% !important;}";
			}
			else if ($position_animate_btn_3 == "4") {
				$dataStr .= "#animate_btn_3 {bottom: 80% !important;}";
			}
			else if ($position_animate_btn_3 == "5") {
				$dataStr .= "#animate_btn_3 {bottom: 45% !important;}";
			}
			else if ($position_animate_btn_3 == "6") {
				$dataStr .= "#animate_btn_3 {bottom: 10% !important;}";
			}
		}

		if (isset($status_animate_btn_4)) {
			if ($position_animate_btn_4 == "1") {
				$dataStr .= "#animate_btn_4 {left:15px; bottom: 80% !important;}";
			}
			else if ($position_animate_btn_4 == "2") {
				$dataStr .= "#animate_btn_4 {left:15px; bottom: 45% !important;}";
			}
			else if ($position_animate_btn_4 == "3") {
				$dataStr .= "#animate_btn_4 {left:15px; bottom: 10% !important;}";
			}
			else if ($position_animate_btn_4 == "4") {
				$dataStr .= "#animate_btn_4 {bottom: 80% !important;}";
			}
			else if ($position_animate_btn_4 == "5") {
				$dataStr .= "#animate_btn_4 {bottom: 45% !important;}";
			}
			else if ($position_animate_btn_4 == "6") {
				$dataStr .= "#animate_btn_4 {bottom: 10% !important;}";
			}
		}

		file_put_contents($file, $dataStr);
	}
}