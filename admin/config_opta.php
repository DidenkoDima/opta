<?php
// HTTP
define('HTTP_SERVER', 'http://opta.promodex.company/admin/');
define('HTTP_CATALOG', 'http://opta.promodex.company/');

// HTTPS
define('HTTPS_SERVER', 'http://opta.promodex.company/admin/');
define('HTTPS_CATALOG', 'http://opta.promodex.company/');

// DIR
define('DIR_APPLICATION', '/home/p/promodex/opta.promodex.company/public_html/admin/');
define('DIR_SYSTEM', '/home/p/promodex/opta.promodex.company/public_html/system/');
define('DIR_IMAGE', '/home/p/promodex/opta.promodex.company/public_html/image/');
define('DIR_LANGUAGE', '/home/p/promodex/opta.promodex.company/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/p/promodex/opta.promodex.company/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/p/promodex/opta.promodex.company/public_html/system/config/');
define('DIR_CACHE', '/home/p/promodex/opta.promodex.company/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/p/promodex/opta.promodex.company/public_html/system/storage/download/');
define('DIR_LOGS', '/home/p/promodex/opta.promodex.company/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/p/promodex/opta.promodex.company/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/p/promodex/opta.promodex.company/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/p/promodex/opta.promodex.company/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'promodex_opta');
define('DB_PASSWORD', 'URz8qWpX');
define('DB_DATABASE', 'promodex_opta');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
