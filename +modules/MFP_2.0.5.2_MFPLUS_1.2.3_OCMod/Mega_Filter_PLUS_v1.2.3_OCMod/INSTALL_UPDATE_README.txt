CONTACT: marsilea15@gmail.com

---------------
--- INSTALL ---
---------------

1. Please make sure that you have installed the latest version of vQmod
	Releases: https://github.com/vqmod/vqmod/releases
	How to install vQmod: https://github.com/vqmod/vqmod/wiki/Installing-vQmod-on-OpenCart

2. Upload all the files and folders to your server from the "Upload" folder. This can be to anywhere of your choice. e.g. /public_html or /public_html/store

3. Go to: System/Users/User Groups

4. Click [Edit] for Top Administrator

5. Click [Select All] for Access Permission and Modify Permission

6. Click SAVE

7. Go to: Extensions/Modules

8. Click [Install] for Mega Filter PRO

9. DONE !

--------------
--- UPDATE ---
--------------

1. Upload all the files and folders to your server from the "Upload" folder. This can be to anywhere of your choice. e.g. /public_html or /public_html/store

2. Go to: Extensions/Modules

3. Click [Edit] for Mega Filter PRO

4. DONE !

EOF