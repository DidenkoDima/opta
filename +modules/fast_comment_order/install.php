<?php 
$sql = "
CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "fast_comment` (
  `fast_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`fast_comment_id`)
) ENGINE=MyIsam DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$this->db->query($sql);
$sql ="
CREATE TABLE IF NOT EXISTS  `" . DB_PREFIX . "fast_comment_description` (
  `fast_comment_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `template_comment` text NOT NULL,
  PRIMARY KEY (`fast_comment_id`,`language_id`)
) ENGINE=MyIsam DEFAULT CHARSET=utf8";
$this->db->query($sql);