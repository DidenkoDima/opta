<?php

// Heading
$_['heading_title'] = 'SEO Package Sitemap';

// Text
$_['text_feed']    = 'Фиды продуктов';
$_['text_success'] = 'Успех: вы изменили Google Sitemap!';
$_['text_info']    = 'Дайте либо полный канал, либо все языковые ссылки для ссылок на Google и поисковые системы.';

// Entry
$_['entry_status']    = 'Статус:';
$_['entry_data_feed'] = 'Полный канал:<span class="help">Канал, содержащий все языки с использованием тега hreflang (hreflang должен быть включен в полном варианте seo)</span>';
$_['entry_lang_feed'] = 'Каналы, основанные на Lang:<span class="help">Если вы предпочитаете версию с раздельными языками, используйте это</span>';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет разрешения на изменение SEO Package Sitemap!';
