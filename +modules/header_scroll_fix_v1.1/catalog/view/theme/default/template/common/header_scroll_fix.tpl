<!-- catalog/view/theme/default/template/common/header_scroll_fix.tpl -->

<section id="header_scroll_fix">
  <nav class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</section>

<style type="text/css">
  #header_scroll_fix{
    background-color:#ffffff;
    border-bottom:2px solid #1f90bb;
    display:none; 
    opacity:1;
    position:fixed; 
    top:0px; 
    width:100%;
    z-index:99;
  }
</style>

<script type="text/javascript">
  jQuery(function () {
    var header_height = jQuery('#header, header').first().height();
    var $header_scroll_fix = jQuery('#header_scroll_fix');
    function headerScrollFix() {
      var scrollY = document.body.scrollTop || document.documentElement.scrollTop || window.scrollY || window.pageYOffset;
      if (scrollY > header_height && !$header_scroll_fix.data('shown')) {
        $header_scroll_fix.stop().slideDown("slow").data('shown', true);
      }
      else if (scrollY < header_height && $header_scroll_fix.data('shown')) {
        $header_scroll_fix.stop().slideUp("slow").data('shown', false);
      }
    }
    jQuery(window).on('scroll', headerScrollFix);
    jQuery(document).on('mousewheel', 'body', headerScrollFix);
  });
</script>