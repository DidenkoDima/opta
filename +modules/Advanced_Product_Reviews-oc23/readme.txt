########################################################################

    ADVANCED PRODUCT REVIEWS //  Version 1.6

########################################################################


------------------------------------------------------------------------
SUPPORT
------------------------------------------------------------------------
Don't hesitate to contact us :

 - Any questions => http://www.adikon.eu/support
 - Support => Skype ID: lee.newways
requirements: openCart 1.5.1 - 2.0.x and vQmod

Single License per website domain.

This extension is not free.
By installing it, you are agreeing to use it only with the applicable number of OpenCart installations for which you purchased it.
You may not use it for more installations than the software license purchased.

This extension is provided for the default installation of OpenCart.
No guarantee is provided that it will work with other customizations you've made.

Copyrights removal is not allowed, unless stated by the original developer.
Written approval (or attached terms of use must) must be obtained from the original owner of the work prior to the removal.


------------------------------------------------------------------------
CHANGELOG
------------------------------------------------------------------------
v1.6 - 27 July 2015
* Added filter on the Review list in admin
* Added field Review title
* Added field Recommend product
* Added modifications to the date of writing a review
* Admin can add images to a comment
* Admin can create predefined Pros/Cons
* Multilingual Reviews (reviews in the current language)
* 'Autoapprove' - Now also reviews written by guests can be automatically accept
* Fixes compatibility for Opencart 2.0.3.x
* Fixed minor bugs

v1.5 - 13 April 2015
* Fixes compatibility for Opencart 2.0.2.x

v1.4 - 21 February 2015
* Compatible with openCart 2.0

v1.3 - 27 November 2014
* Added: Comments by administrator
* Added: Seo url for the page with all the reviews
* Added: Separate reviews by each store
* Added: Share review
* Added: Summary block on the product page
* Added: Disable captcha if customer is logged
* Added: Bonus points for writing reviews
* Added: The customer can not post twice reviews for the same product
* Added: Customers who bought the product can write a review
  and more

v1.2 - 17 July 2014
* Adding: Image to the review
* Added: Average rating of review
* Fixed minor bugs

v1.1 - 28 January 2014
* Fixed minor bugs

v1.0 - 13 October 2013
* Initial release


Thank you,
Adikon Team