<?php
// Entry
$_['entry_review_title'] = 'Review title';
$_['entry_add_pros'] = 'Add pros';
$_['entry_add_cons'] = 'Add cons';
$_['entry_review_image'] = 'Add the image to the presentation (max %s):';
$_['entry_recommend_product'] = 'I would recommend this product to a friend?';

// Text
$_['text_reply'] = 'Reply by';
$_['text_pros'] = 'Pros';
$_['text_cons'] = 'Cons';
$_['text_on'] = 'on';
$_['text_yes'] = 'Yes';
$_['text_no'] = 'No';
$_['text_average_review'] = 'Total:';
$_['text_product_review'] = 'Reviews about %s';
$_['text_general_avarage'] = 'Average rating:';
$_['text_count_recommend_product'] = '<b>%s of %s (%s)</b> reviewers would<br />recommend this product to a friend.';
$_['text_general_count_mark'] = '<b>Total reviews:</b> %s<br />Write your own review on this product.';
$_['text_report_abuse'] = 'Report abuse';
$_['text_report_it'] = 'Report it';
$_['text_other_reason'] = 'Other (write below)';
$_['text_please_wait'] = 'Please wait!';
$_['text_helpfull_percentage'] = 'Was this review is helpful? <button class="vote_yes" data-vote="1" data-review-id="%s">Yes</button><button class="vote_no" data-vote="0" data-review-id="%s">No</button> %s found this review helpful.';
$_['text_helpfull_numerically'] = 'Was this review is helpful? <button class="vote_yes" data-vote="1" data-review-id="%s">Yes</button><button class="vote_no" data-vote="0" data-review-id="%s">No</button> %s of %s people found this review helpful.';
$_['text_share_title'] = '%s review of %s';
$_['text_success_helpfull_percentage_yes'] = 'In your opinion is useful. %s found this review helpful.';
$_['text_success_helpfull_percentage_no'] = 'In your opinion is useless. %s found this review helpful.';
$_['text_success_helpfull_numerically_yes'] = 'In your opinion is useful. %s of %s found this review helpful.';
$_['text_success_helpfull_numerically_no'] = 'In your opinion is useless. %s of %s found this review helpful.';
$_['text_report_abuse_success'] = 'Your report has been successfully sent. Thank you.';
$_['text_sort'] = 'Sort by:';
$_['text_default'] = 'Default';
$_['text_rating_desc'] = 'Rating (highest)';
$_['text_rating_asc'] = 'Rating (lowest)';
$_['text_helpfull_desc'] = 'Helpfull (highest)';
$_['text_helpfull_asc'] = 'Helpfull (lowest)';
$_['text_date_added_desc'] = 'Date (new)';
$_['text_date_added_asc'] = 'Date (old)';
$_['text_upload'] = 'Your file was successfully uploaded!';

// Buttons
$_['button_write_review'] = 'Write a review';

// Error
$_['error_filename'] = 'Filename must be between 3 and 64 characters!';
$_['error_filetype'] = 'Invalid file type!';
$_['error_upload'] = 'Upload required!';
$_['error_logged_guest_rate'] = 'You must be logged in to rate this product!';
$_['error_logged_helpfull'] = 'You must be logged if you want to vote the review is helpfull!';
$_['error_logged_report_abuse'] = 'You must be logged in to report abuse!';
$_['error_report_abuse'] = 'Please choose title the report!';
$_['error_def_report_abuse'] = 'Please give a reason the report!';
$_['error_already_helpfull'] = 'You have already voted!';
$_['error_helpfull'] = 'The vote was not counted. Please try again later!';
$_['error_pros_cons_limit'] = 'Pros and cons must have between %s and %s characters!';
$_['error_purchase_product'] = 'To write a review, you must first buy this product!';
$_['error_already_review_product'] = 'Already you wrote a review for this product!';
$_['error_review_title'] = 'Review Title must be between 3 and 40 characters!';