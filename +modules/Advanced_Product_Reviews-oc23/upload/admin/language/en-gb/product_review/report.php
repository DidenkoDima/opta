<?php
// Heading
$_['heading_title']        = 'Abuse reports';
$_['heading_title_reason'] = 'Reasons report';

// Text
$_['text_success_report'] = 'Success: You have modified Abuse reports!';
$_['text_success_reason'] = 'Success: You have modified Reason reports!';
$_['text_list']           = 'Reasons report list';
$_['text_abuse_list']     = 'Abuse report list';
$_['text_add']            = 'Add';
$_['text_edit']           = 'Edit';
$_['text_default']        = 'Default';
$_['text_delete']         = 'Delete';
$_['text_confirm']        = 'Are you sure?';

// Button
$_['button_reason']        = 'Manage reasons';
$_['button_delete_review'] = 'Delete with reviews';
$_['button_add']           = 'Add';

// Column
$_['column_review']       = 'Review';
$_['column_title']        = 'Title';
$_['column_store']        = 'Store';
$_['column_reported']     = 'Reported by';
$_['column_date_added']   = 'Date added';
$_['column_status']       = 'Status';
$_['column_action']       = 'Action';

// Entry
$_['entry_name'] 	      = 'Name';
$_['entry_store']         = 'Store';
$_['entry_date_start']    = 'Date start';
$_['entry_date_stop']     = 'Date stop';
$_['entry_status']        = 'Status';

// Error
$_['error_name']          = 'Reason name must be between 3 and 255 characters!';
$_['error_permission']    = 'Warning: You do not have permission to modify Abuse reports!';
?>