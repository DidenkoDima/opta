CREATE TABLE IF NOT EXISTS `oc_pr_attribute` (
					`attribute_id` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(255) NOT NULL,
					`type` tinyint(1) NOT NULL,
					`added_by` varchar(1) NOT NULL DEFAULT 'u',
					`predefined` tinyint(1) NOT NULL DEFAULT '0',
					`review_id` int(11) NOT NULL,
					`status` tinyint(1) NOT NULL,
					PRIMARY KEY (`attribute_id`),
					KEY `review_id` (`review_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_rating` (
					`rating_id` int(11) NOT NULL AUTO_INCREMENT,
					`sort_order` int(3) NOT NULL DEFAULT '0',
					`status` tinyint(1) NOT NULL,
					PRIMARY KEY (`rating_id`),
					UNIQUE KEY `rating_id` (`rating_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_rating_description` (
					`rating_id` int(11) NOT NULL,
					`language_id` int(11) NOT NULL,
					`name` varchar(64) NOT NULL,
					PRIMARY KEY (`rating_id`,`language_id`),
					KEY `name` (`name`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_rating_review` (
					`rating_review_id` int(11) NOT NULL AUTO_INCREMENT,
					`review_id` int(11) NOT NULL,
					`rating_id` int(11) NOT NULL,
					`rating` tinyint(1) NOT NULL,
					PRIMARY KEY (`rating_review_id`,`rating_id`),
					KEY `review_id` (`review_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_rating_to_store` (
					`rating_id` int(11) NOT NULL,
					`store_id` int(11) NOT NULL,
					PRIMARY KEY (`rating_id`,`store_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_reason` (
					`reason_id` int(11) NOT NULL AUTO_INCREMENT,
					`status` tinyint(1) NOT NULL,
					PRIMARY KEY (`reason_id`),
					UNIQUE KEY `reason_id` (`reason_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_reason_description` (
					`reason_id` int(11) NOT NULL,
					`language_id` int(11) NOT NULL,
					`name` varchar(255) NOT NULL,
					PRIMARY KEY (`reason_id`,`language_id`),
					KEY `name` (`name`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_reason_to_store` (
					`reason_id` int(11) NOT NULL,
					`store_id` int(11) NOT NULL,
					PRIMARY KEY (`reason_id`,`store_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_report` (
					`report_id` int(11) NOT NULL AUTO_INCREMENT,
					`title` varchar(255) NOT NULL,
					`reported` varchar(94) NOT NULL,
					`review_id` int(11) NOT NULL,
					`customer_id` int(11) NOT NULL,
					`store_id` int(11) NOT NULL,
					`date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					PRIMARY KEY (`report_id`,`store_id`),
					UNIQUE KEY `report_id` (`report_id`),
					KEY `review_id` (`review_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_review_image` (
					`review_image_id` int(11) NOT NULL AUTO_INCREMENT,
					`review_id` int(11) NOT NULL,
					`image` varchar(255) NOT NULL DEFAULT '',
					PRIMARY KEY (`review_image_id`,`review_id`),
					KEY `review_id` (`review_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `oc_pr_comment_image` (
					`comment_image_id` int(11) NOT NULL AUTO_INCREMENT,
					`review_id` int(11) NOT NULL,
					`image` varchar(255) NOT NULL DEFAULT '',
					PRIMARY KEY (`comment_image_id`,`review_id`),
					KEY `review_id` (`review_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `oc_review` ADD `language_id` INT(9) NOT NULL DEFAULT '0';
ALTER TABLE `oc_review` ADD `title` VARCHAR(40) NOT NULL;
ALTER TABLE `oc_review` ADD `recommend` VARCHAR(1) NOT NULL DEFAULT 'y';
ALTER TABLE `oc_pr_attribute` ADD `added_by` VARCHAR(1) NOT NULL DEFAULT 'u';
ALTER TABLE `oc_pr_attribute` ADD `predefined` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `oc_review` ADD `vote_yes` INT(9) NOT NULL DEFAULT '0';
ALTER TABLE `oc_review` ADD `vote_no` INT(9) NOT NULL DEFAULT '0';
ALTER TABLE `oc_review` ADD `comment` text COLLATE utf8_bin NOT NULL;
ALTER TABLE `oc_review` ADD `comment_date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `oc_review` ADD `store_id` INT(11) NOT NULL DEFAULT '0';