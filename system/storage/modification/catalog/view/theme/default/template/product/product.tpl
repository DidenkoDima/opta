<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

				<?php if(!$config_file_block_positions or ($config_file_block_positions == 'file_block_position_top' and $file_to_product and !$config_file_block_custom_positions_block)) echo $file_to_product;  ?>
			
      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <?php if ($thumb || $images) { ?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <li class="image-additional"><a class="thumbnail" <?php if($image['video']){?> data-video="<?php echo $image['video']; ?>" <?php } ?> href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php } ?>
          </ul>

				<?php if($config_file_block_positions == 'file_block_position_after_images' and $file_to_product and !$config_file_block_custom_positions_block) echo $file_to_product;  ?>
			
          <?php } ?>

				<?php if($config_file_block_positions == 'file_block_position_before_tabs' and $file_to_product and !$config_file_block_custom_positions_block) echo $file_to_product;  ?>
			
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
			<?php foreach($product_tabs as $key => $tab){ ?>
			<li><a href="#tab-<?php echo $product_id ?>-<?php echo $tab['product_tab_id']; ?>" data-toggle="tab"><?php echo $tab['title']; ?></a></li>
			<?php } ?>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>

				<?php if($config_file_block_positions == 'file_block_position_in_tabs' and $file_to_product and !$config_file_block_custom_positions_block){ ?>
				  <li><a href="#tab-file" data-toggle="tab"><?php echo $tab_file; ?></a></li>
				<?php } ?>
			
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
			<?php foreach($product_tabs as $key => $tab){ ?>
			<div class="tab-pane" id="tab-<?php echo $product_id ?>-<?php echo $tab['product_tab_id']; ?>"><?php echo $tab['description']; ?></div>
			<?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
<?php if ($config->get('product_reviews_status')) { ?>
			<?php if ($config->get('product_reviews_summary_status')) { ?>
			<div class="product_review_summary">
				<div class="product_name"><?php echo $text_product_review; ?></div>
				<div class="left">
					<div class="general_avarage"><?php echo $text_general_avarage; ?> <img src="image/product_review/stars-<?php echo $config->get('product_reviews_appearance_customer_rating'); ?>-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" /></div>
					<div class="count_mark"><?php echo $text_general_count_mark; ?></div>
					<a onClick="$('html, body').animate({ scrollTop: $('input[name=\'name\']').offset().top }, 2000);" class="button"><?php echo $button_write_review; ?></a>
					<?php if ($recommend_total && $config->get('product_reviews_recommend_status')) { ?>
					<div class="count_recommend_product"><?php echo $text_count_recommend_product; ?></div>
					<?php } ?>
				</div>
				<div class="right">
					<?php if ($total_ratings) { ?>
					<div class="product_rating_list">
						<ul>
							<?php $sum_rating = 0; ?>
							<?php $sum_total = 0; ?>
							<?php foreach ($total_ratings as $product_rating) { ?>
							<?php $rating = $product_rating['sum_rating'] / $product_rating['total']; ?>
							<li><?php echo $product_rating['name']; ?><img src="image/product_review/stars-<?php echo $config->get('product_reviews_appearance_customer_rating'); ?>-<?php echo round($rating, 0); ?>.png" alt="<?php echo round($rating, 0); ?>" /></li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>
				</div>
				<div style="clear: both;"></div>
			</div>
			<?php } ?>
			<?php if ($config->get('product_reviews_sort_status')) { ?>
			<div class="product_review_sort"><b><?php echo $text_sort; ?></b>
				<select onchange="$('#review').fadeOut('slow'); $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>&sort=' + this.value); $('#review').fadeIn('slow');">
					<?php foreach ($product_review_sorts as $sorts) { ?>
					<option value="<?php echo $sorts['value']; ?>"><?php echo $sorts['text']; ?></option>
					<?php } ?>
				</select>
			</div>
			<?php } ?>
			<?php } ?>
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>

			<?php if ($config->get('product_reviews_status')) { ?>
			<?php if ($config->get('product_reviews_review_title_status')) { ?>
			<div class="form-group required">
			  <div class="col-sm-12">
			    <label class="control-label" for="input-review-title"><?php echo $entry_review_title; ?></label>
			    <input type="text" name="review_title" value="" id="input-review-title" class="form-control" />
			  </div>
			</div>
			<?php } ?>
			<?php } ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; 
              <?php if ($config->get('product_reviews_status')) {
				if ($ratings) {
					echo'<table class="product_rating">';
					foreach ($ratings as $rating) {
						echo'<tr><td>' . $rating['name'] . '</td><td><input type="hidden" name="rating[' . $rating['rating_id'] . ']" value="" /><div class="rating_star2" data-average="5" data-id="' . $rating['rating_id'] . '"></div></td></tr>';
					}
					echo'</table>';
                }
              
                if ($config->get('product_reviews_pros_status') || $config->get('product_reviews_cons_status')) {
                echo'<table class="pros_cons">';
                echo'<tr>';

                if ($config->get('product_reviews_pros_status') || $config->get('product_reviews_predefined_pros_cons_status')) {
                echo'<td class="pros" style="width:50%!important;">' . $entry_add_pros . '</td>';
                }

                if ($config->get('product_reviews_cons_status') || $config->get('product_reviews_predefined_pros_cons_status')) {
                echo'<td class="cons">' . $entry_add_cons . '</td>';
                }

                echo'</tr>';
                echo'<tr>';

                echo'<td>';
                if ($predefined_pros && $config->get('product_reviews_predefined_pros_cons_status')) {
                    foreach ($predefined_pros as $pros) {
                        echo'<div class="predefined_pros_cons"><input type="checkbox" name="predefined_pros[]" value="' . base64_encode($pros['name']) . '" /> ' . $pros['name'] . '</div>';
                    }
                }

                if ($config->get('product_reviews_pros_status')) {
                echo'<input type="text" name="review_pros[]" />';
                }
                echo'</td>';

                echo'<td>';
                if ($predefined_cons && $config->get('product_reviews_predefined_pros_cons_status')) {
                    foreach ($predefined_cons as $cons) {
                        echo'<div class="predefined_pros_cons"><input type="checkbox" name="predefined_cons[]" value="' . base64_encode($cons['name']) . '" /> ' . $cons['name'] . '</div>';
                    }
                }

                if ($config->get('product_reviews_cons_status')) {
                echo'<input type="text" name="review_cons[]" />';
                }
                echo'</td>';

                echo'</tr>';
                echo'</table>';
                }

                if ($config->get('product_reviews_image_status')) {
                echo'<div style="margin-top: 20px;">' . $entry_review_image . ' <span class="left-button-org"><span class="right-button-org"><input type="button" value="' . $button_upload . '" id="review_image" class="button"></span></span><div id="review_images"></div></div>';
                }

                if ($config->get('product_reviews_recommend_status')) {
                echo'<div style="margin-top: 20px;">' . $entry_recommend_product . ' <select name="recommend" class="form-control"><option value="y">' . $text_yes . '</option><option value="n">' . $text_no . '</option></select></div>';
                }
            } else { ?>
            <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></span><?php } ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>

			  <?php if($config_file_block_positions == 'file_block_position_in_tabs' and $file_to_product and !$config_file_block_custom_positions_block){ ?>
			  <div class="tab-pane" id="tab-file">
				<?php echo $file_to_product; ?>
			  </div>
			  <?php } ?> 
			
          </div>
        </div>
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-4'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="btn-group">
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
          </div>
          <h1><?php echo $heading_title; ?></h1>
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
            <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
          </ul>
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if (!$special) { ?>
            <li>
              <h2><?php echo $price; ?></h2>
            </li>
            <?php } else { ?>
            <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
            <li>
              <h2><?php echo $special; ?></h2>
            </li>
            <?php } ?>
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <div id="product">

				<?php if($config_file_block_positions == 'file_block_position_before_options' and $file_to_product and !$config_file_block_custom_positions_block) echo $file_to_product; ?>
			
            <?php if ($options) { ?>
            <hr>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>                    
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring; ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group">
              <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
<div class="input-group number-spinner">
			      <span class="input-group-btn">
					<button class="btn btn-default" data-dir="dwn"><i class="fa fa-minus"></i></button>
			      </span>
				<input type="text" name="quantity" value="<?php echo $minimum; ?>" id="input-quantity" class="form-control text-center" />
				  <span class="input-group-btn">
					<button class="btn btn-default" data-dir="up"><i class="fa fa-plus"></i></button>
				  </span>
			    </div>
              
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
          <?php if ($review_status) { ?>
          <div class="rating">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p>
            <hr>
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
            <!-- AddThis Button END -->
          </div>
          <?php } ?>
        </div>
      </div>

				<?php if($config_file_block_positions == 'file_block_position_after_tabs' and $file_to_product and !$config_file_block_custom_positions_block){ ?>
				  <?php echo $file_to_product; ?>
				<?php } ?>
			
      <?php if ($products) { ?>
      <h3><?php echo $text_related; ?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-xs-8 col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-xs-6 col-md-4'; ?>
        <?php } else { ?>
        <?php $class = 'col-xs-6 col-sm-3'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($j = 1; $j <= 5; $j++) { ?>
                <?php if ($product['rating'] < $j) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            <div class="button-group">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
          </div>
        </div>
        <?php if (($column_left && $column_right) && (($i+1) % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && (($i+1) % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif (($i+1) % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				
			<?php if ($config->get('product_reviews_status')) { ?>
			$('input[name^=\'review_pros\']').each(function(index) {
				if (!$(this).is(':last-child')) {
					$(this).remove();
				}
			});

			$('input[name^=\'review_cons\']').each(function(index) {
				if (!$(this).is(':last-child')) {
					$(this).remove();
				}
			});

			$('input[name^=\'rating\']').each(function(index) {
				$(this).val('');
			});
			<?php } ?>
			
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a:not([data-video])',
		gallery: {
			enabled:true
		}
	});
});
//--></script>

				<?php if($file and $config_file_block_custom_positions_block){ ?>
				  <script>
					$(document).ready(function(){
						$('<?php echo $config_file_block_custom_positions_block; ?>').<?php echo $config_file_block_custom_positions; ?>($('#files_block').html());
					});
				  </script>
				  <div id="files_block" style="display:none;"><?php echo $file_to_product; ?></div>
				<?php } ?>
			
<?php if ($config->get('product_reviews_status')) {
			echo'<style>' . html_entity_decode($config->get('product_reviews_form_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_list_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_pros_cons_list_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_rating_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_helpfulness_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_sort_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_total_rating_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_image_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_comment_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_summary_css'), ENT_QUOTES, 'UTF-8') . "\n" . html_entity_decode($config->get('product_reviews_share_css'), ENT_QUOTES, 'UTF-8') . '</style>';
			} ?>

			  <?php if ($config->get('product_reviews_status')) { ?>
			  <?php if ($config->get('product_reviews_report_abuse_status')) { ?>
			  <input type="hidden" name="r_id" value="" />
			  <?php if (version_compare(VERSION, '2.0') < 0) { ?>
			  <div id="dialog-report-abuse" title="<?php echo $text_report_abuse; ?>">
			    <p class="validateTips"></p>
				<?php foreach ($reasons as $reason) { ?>
			    <input type="radio" name="reason_id" value="<?php echo $reason['reason_id']; ?>" /> <?php echo $reason['name']; ?><br />
			    <?php } ?>
			    <input type="radio" name="reason_id" value="0" /> <?php echo $text_other_reason; ?><br /><input type="text" name="other" value="" class="ui-widget-content ui-corner-all" style="margin: 3px 0 0 25px;" />
			  </div>
			  <?php } else { ?>
			  <div class="modal fade" id="dialog-report-abuse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
				  <div class="modal-content">
				    <div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <h4 class="modal-title" id="myModalLabel"><?php echo $text_report_abuse; ?></h4>
				    </div>
				    <div class="modal-body">
					  <p class="validateTips"></p>
					  <?php foreach ($reasons as $reason) { ?>
					  <input type="radio" name="reason_id" value="<?php echo $reason['reason_id']; ?>" /> <?php echo $reason['name']; ?><br />
					  <?php } ?>
					  <input type="radio" name="reason_id" value="0" /> <?php echo $text_other_reason; ?><br /><input type="text" name="other" value="" class="form-control" style="width: 200px; margin: 3px 0 0 25px;" />
				    </div>
				    <div class="modal-footer">
				      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				      <button type="button" class="btn btn-primary" id="button-reason-send"><?php echo $button_continue; ?></button>
				    </div>
			      </div>
			    </div>
			  </div>
			  <?php } ?>
			  <?php } ?>

			  <?php if ($config->get('product_reviews_image_status')) { ?>
			  <?php if (version_compare(VERSION, '2.0') < 0) { ?>
			  <script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
			  <?php } ?>

			  <?php } ?>
			  <script type="text/javascript">
			  $(document).ready(function(){
			    <?php if ($config->get('product_reviews_report_abuse_status')) { ?>
				$(document).delegate('a#report_abuse', 'click', function() {
				  <?php if ($config->get('product_reviews_report_abuse_guest') && !$customer->isLogged()) { ?>
				  alert('<?php echo $error_logged_report_abuse; ?>');
				  <?php } else { ?>
				  $('input[name="r_id"]').val($(this).attr("rel"));

				  <?php if (version_compare(VERSION, '2.0') < 0) { ?>
				  $("#dialog-report-abuse").dialog("open");
				  <?php } else { ?>
				  $("#dialog-report-abuse").modal('show');
				  <?php } ?>
				  <?php } ?>
				});

				<?php if (version_compare(VERSION, '2.0') < 0) { ?>
				 $("#dialog-report-abuse").dialog({
				  autoOpen: false,
				  height: 300,
				  width: 350,
				  modal: true,
				  buttons: {
				    "<?php echo $button_continue; ?>": function() {
					  $.ajax({
					    url: 'index.php?route=product/allreviews/reportabuse&review_id=' + $('input[name="r_id"]').val(),
						type: 'post',
						dataType: 'json',
						data: 'reason_id=' + encodeURIComponent($('input[name=\'reason_id\']:checked').val()) + '&def=' + encodeURIComponent($('input[name=\'other\']').val()),
						beforeSend: function() {
						  $(".validateTips").text("").removeClass("ui-state-highlight ui-state-error ui-state-success");
						},
						complete: function() { },
						success: function(data) {
						  if (data['error']) {
						    $(".validateTips").text(data['error']).addClass("ui-state-highlight ui-state-error").css('padding', '10px');
						  }

						  if (data['success']) {
							$(".validateTips").text(data['success']).addClass("ui-state-highlight ui-state-success").css('padding', '10px');

							$("#dialog-report-abuse").siblings('.ui-dialog-buttonpane').find('button:first').hide();
						  }
						}
					  });
				    },
				    Cancel: function() {
				      $(this).dialog("close");
				    }
				  },
				  close: function() {
					$(".validateTips").text("").removeClass("ui-state-highlight ui-state-error ui-state-success").css('padding', '0px');
					$("#dialog-report-abuse").siblings('.ui-dialog-buttonpane').find('button:first').show();
					$('input[name=\'other\']').val('');
				  }
				});
				<?php } else { ?>
				$(document).delegate('#button-reason-send', 'click', function() {
				  if ($('input[name=\'reason_id\']:checked').length <= 0) {
				    return;
				  }

				  $.ajax({
				    url: 'index.php?route=product/allreviews/reportabuse&review_id=' + $('input[name="r_id"]').val(),
				    type: 'post',
				    dataType: 'json',
				    data: 'reason_id=' + encodeURIComponent($('input[name=\'reason_id\']:checked').val()) + '&def=' + encodeURIComponent($('input[name=\'other\']').val()),
				    beforeSend: function() {
				      $(".validateTips").html("");
				    },
				    complete: function() { },
				    success: function(data) {
					  if (data['error']) {
					    $(".validateTips").html('<div class="alert alert-danger">' + data['error'] + '</div>').css('padding', '10px');
					  }

					  if (data['success']) {
					    $(".validateTips").html('<div class="alert alert-success">' + data['success'] + '</div>').css('padding', '10px');

					    $("#dialog-report-abuse").find('#button-reason-send').hide();
					  }
				    }
				  });
				});
				<?php } ?>
				<?php } ?>

				function isMobileDetect() {
					return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|android|ipad|playbook|silk/i.test(navigator.userAgent||navigator.vendor||window.opera)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test((navigator.userAgent||navigator.vendor||window.opera).substr(0,4)))
				}

				$('.rating_star2').jRating({
					smallStarsPath: 'catalog/view/javascript/jrating-master/jquery/icons/small.png',
					mediumStarsPath: 'catalog/view/javascript/jrating-master/jquery/icons/medium.png',
					bigStarsPath: 'catalog/view/javascript/jrating-master/jquery/icons/stars.png',
					phpPath: '<?php echo HTTP_SERVER; ?>catalog/view/javascript/jrating-master/php/jrating.php',
					step: true,
					type: '<?php echo $config->get('product_reviews_appearance_type'); ?>',
					length: 5,
					rateMax: 5,
					showRateInfo: false,
					canRateAgain: true,
					nbRates: 7,
					onClick : function(element, rate) {
						if (isMobileDetect()) {
							$('input[name="rating[' + $(element).attr('data-id') + ']"]').val(rate);
						} else {
							$('input[name="rating[' + $(element).parent('div').attr('data-id') + ']"]').val(rate);
						}
					},
					onTouchstart : function(element, rate) {
						if (isMobileDetect()) {
							$('input[name="rating[' + $(element).attr('data-id') + ']"]').val(rate);
						} else {
							$('input[name="rating[' + $(element).parent('div').attr('data-id') + ']"]').val(rate);
						}
					}
				});

				<?php if ($config->get('product_reviews_helpfulness_status')) { ?>
				$(document).delegate('.product_review_vote button', 'click', function() {
					<?php if ($config->get('product_reviews_helpfulness_guest') && !$customer->isLogged()) { ?>
					alert('<?php echo $error_logged_helpfull; ?>');
					<?php } else { ?>
					var helpfull_box = $(this).parents('.product_review_helpfulness').find('span:first');
					var helpfull_box_copy = helpfull_box.html();

					$.ajax({
						url: 'index.php?route=product/allreviews/vote&product_id=<?php echo $request->get['product_id']?>',
						type: 'post',
						dataType: 'json',
						data: 'vote=' + encodeURIComponent($(this).attr('data-vote')) + '&review_id=' + encodeURIComponent($(this).attr('data-review-id')),
						beforeSend: function() {
							<?php if (version_compare(VERSION, '2.0') < 0) { ?>
							helpfull_box.html('<img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_please_wait; ?>');
							<?php } else { ?>
							helpfull_box.html('<i class="fa fa-spin fa-spinner"></i> <?php echo $text_please_wait; ?>');
							<?php } ?>
						},
						complete: function() { },
						success: function(data) {
							if (data['error']) {
								alert(data['error']);

								helpfull_box.html(helpfull_box_copy);
							}

							if (data['success']) {
								helpfull_box.html(data['success']);
							}
						}
					});
					<?php } ?>
				});
				<?php } ?>
			});

			var pros_cons_limit = '<?php echo (int)$config->get('product_reviews_pros_cons_limit'); ?>';

			$('.pros_cons').on('keyup', 'input[name^="review_pros"], input[name^="review_cons"]', function() {
				name = $(this).attr('name').replace('[]', '');

				if (this.value != '') {
					if ($('input[name^="' + name + '"]:last').val() != '' && $('input[name^="' + name + '"]').length < pros_cons_limit) {
						$('input[name^="' + name + '"]:last').after('<input type="text" name="' + name + '[]" />');
					}
				} else {
					if (this.value == '' && $('input[name^="' + name + '"]').length > 1) {
						if (!$(this).is(':last-child')) {
							$(this).remove();
						}

						if ($('input[name^="' + name + '"]:last').val() != '') {
							$('input[name^="' + name + '"]:last').after('<input type="text" name="' + name + '[]" />');

							$('input[name^="' + name + '"]:last').focus();
						}
					}
				}
			});

			<?php if ($config->get('product_reviews_image_status')) { ?>
			<?php if (version_compare(VERSION, '2.0') < 0) { ?>
			new AjaxUpload('#review_image', {
				action: 'index.php?route=product/allreviews/reviewimageupload',
				name: 'file',
				autoSubmit: true,
				responseType: 'json',
				onSubmit: function(file, extension) {
					$('#review_image').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
					$('#review_image').attr('disabled', true);
				},
				onComplete: function(file, json) {
					$('#review_image').attr('disabled', false);

					$('.error').remove();

					if (json['success']) {
						alert(json['success']);

						$('#review_images').append('<div class="rimage"><img src="' + json['thumb'] + '" alt="" /><input type="hidden" name="review_images[]" value="' + json['file'] + '" /></div>');

						if ($('#review_images > div').length >= <?php echo (int)$config->get('product_reviews_image_limit'); ?>) {
							$('#review_image').remove();
						}
					}

					if (json['error']) {
						$('#review_image').after('<span class="error">' + json['error'] + '</span>');
					}

					$('.loading').remove();	
				}
			});
			<?php } else { ?>
			$('#review_image').on('click', function() {
				var node = this;

				$('#form-upload').remove();

				$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

				$('#form-upload input[name=\'file\']').trigger('click');

				timer = setInterval(function() {
					if ($('#form-upload input[name=\'file\']').val() != '') {
						clearInterval(timer);

						$.ajax({
							url: 'index.php?route=product/allreviews/reviewimageupload',
							type: 'post',
							dataType: 'json',
							data: new FormData($('#form-upload')[0]),
							cache: false,
							contentType: false,
							processData: false,
							beforeSend: function() {
								$(node).button('loading');
							},
							complete: function() {
								$(node).button('reset');
							},
							success: function(json) {
								$('.text-danger').remove();
					
								if (json['error']) {
									$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
								}
					
								if (json['success']) {
									alert(json['success']);

									$('#review_images').append('<div class="rimage"><img src="' + json['thumb'] + '" alt="" /><input type="hidden" name="review_images[]" value="' + json['file'] + '" /></div>');

									if ($('#review_images > div').length >= <?php echo (int)$config->get('product_reviews_image_limit'); ?>) {
										$(node).remove();
									}
								}
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					}
				}, 500);
			});
			<?php } ?>
			<?php } ?>
			</script>
			<?php } ?>
			


                <?php if($video_status){  ?>

                    <script type="text/javascript">
                        window.onload  = function() {                    
                            jQuery('a[data-video]:not([data-video=""])').each(function(index,element) {
                                jQuery(this).attr('href', $(this).attr('data-video'));
                                jQuery(this).attr('target','_blank');
                                jQuery(this).css({'background-repeat':'no-repeat','background-position':'center center', 'background-size': '100%', 'background-image': 'url("/image/play.png")'}).find('img').css({ opacity: 0.6 });
                            });
                            // jQuery('a[data-video]:not([data-video=""]):not([data-video_role="video_main"]) img').css({ width: '74px', height: '74px' });
                            jQuery('a[data-video][data-video_role="video_main"]:not([data-video=""])').css({ 'background-image': 'none' });
                        }
                        jQuery(document).ready(function() {
                        	if(jQuery('a[data-video]:not([data-video=""]').length){
								jQuery('a[data-video]:not([data-video=""]').magnificPopup({
									disableOn: 700,
									type: 'iframe',
									mainClass: 'mfp-fade',
									removalDelay: 160,
									preloader: false,
									fixedContentPos: false
								});
							}
						});
                    </script>


                <?php } ?>
            
<script type="text/javascript"><!--
$(document).on('click', '.number-spinner button', function () {    
	var btn = $(this),
		oldValue = btn.closest('.number-spinner').find('input').val().trim(),
		newVal = 1;
			
	if (btn.attr('data-dir') == 'up') {
		newVal = parseInt(oldValue) + 1;
	} else {
		if (oldValue > 1) {
			newVal = parseInt(oldValue) - 1;
		} else {
			newVal = 1;
		}
	}
	btn.closest('.number-spinner').find('input').val(newVal);
});
//--></script>
<?php echo $footer; ?>
