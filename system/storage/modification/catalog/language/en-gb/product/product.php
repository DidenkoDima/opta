<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_search']              = 'Search';
$_['text_brand']               = 'Brand';
$_['text_manufacturer']        = 'Brand:';
$_['text_model']               = 'Product Code:';
$_['text_reward']              = 'Reward Points:';
$_['text_points']              = 'Price in reward points:';
$_['text_stock']               = 'Availability:';
$_['text_instock']             = 'In Stock';
$_['text_tax']                 = 'Ex Tax:';
$_['text_discount']            = ' or more ';
$_['text_option']              = 'Available Options';
$_['text_minimum']             = 'This product has a minimum quantity of %s';

			$_['error_logged_guest_rate']   = 'You must be logged in to rate this product!';
			$_['error_logged_helpfull']     = 'You must be logged if you want to vote the review is helpfull!';
			$_['error_logged_report_abuse'] = 'You must be logged in to report abuse!';
			$_['error_report_abuse']        = 'Please choose title the report!';
			$_['error_def_report_abuse']    = 'Please give a reason the report!';
			$_['error_already_helpfull']    = 'You have already voted!';
			$_['error_helpfull']            = 'The vote was not counted. Please try again later!';
			$_['error_pros_cons_limit']     = 'Pros and cons must have between %s and %s characters!';
			$_['error_purchase_product']    = 'To write a review, you must first buy this product!';
			$_['error_already_review_product'] = 'Already you wrote a review for this product!';
			$_['error_review_title']        = 'Review Title must be between 3 and 40 characters!';
			$_['entry_review_title']        = 'Review title';
			$_['entry_add_pros']            = 'Add pros';
			$_['entry_add_cons']            = 'Add cons';
			$_['entry_review_image']        = 'Add the image to the presentation (max %s):';
			$_['entry_recommend_product']   = 'I would recommend this product to a friend?';
			$_['text_reply']                = 'Reply by';
			$_['text_pros']                 = 'Pros';
			$_['text_cons']                 = 'Cons';
			$_['text_on']                   = 'on';
			$_['text_yes']                  = 'Yes';
			$_['text_no']                   = 'No';
			$_['text_average_review']       = 'Total:';
			$_['text_product_review']       = 'Reviews about %s';
			$_['text_general_avarage']      = 'Average rating:';
			$_['text_count_recommend_product'] = '<b>%s of %s (%s)</b> reviewers would<br />recommend this product to a friend.';
			$_['text_general_count_mark']   = '<b>Total reviews:</b> %s<br />Write your own review on this product.';
			$_['text_report_abuse']         = 'Report abuse';
			$_['text_report_it']            = 'Report it';
			$_['text_other_reason']         = 'Other (write below)';
			$_['text_please_wait']          = 'Please wait!';
			$_['text_helpfull_percentage']  = 'Was this review is helpful? <button class="vote_yes" data-vote="1" data-review-id="%s">Yes</button><button class="vote_no" data-vote="0" data-review-id="%s">No</button> %s found this review helpful.';
			$_['text_helpfull_numerically'] = 'Was this review is helpful? <button class="vote_yes" data-vote="1" data-review-id="%s">Yes</button><button class="vote_no" data-vote="0" data-review-id="%s">No</button> %s of %s people found this review helpful.';
			$_['text_share_title']          = '%s review of %s';
			$_['text_success_helpfull_percentage_yes']  = 'In your opinion is useful. %s found this review helpful.';
			$_['text_success_helpfull_percentage_no']   = 'In your opinion is useless. %s found this review helpful.';
			$_['text_success_helpfull_numerically_yes'] = 'In your opinion is useful. %s of %s found this review helpful.';
			$_['text_success_helpfull_numerically_no']  = 'In your opinion is useless. %s of %s found this review helpful.';
			$_['text_report_abuse_success'] = 'Your report has been successfully sent. Thank you.';
			$_['text_sort']                 = 'Sort by:';
			$_['text_default']              = 'Default';
			$_['text_rating_desc']          = 'Rating (highest)';
			$_['text_rating_asc']           = 'Rating (lowest)';
			$_['text_helpfull_desc']        = 'Helpfull (highest)';
			$_['text_helpfull_asc']         = 'Helpfull (lowest)';
			$_['text_date_added_desc']      = 'Date (new)';
			$_['text_date_added_asc']       = 'Date (old)';
			$_['button_write_review']       = 'Write a review';
			$_['error_filename']            = 'Filename must be between 3 and 64 characters!';
			$_['error_filetype']            = 'Invalid file type!';
			$_['error_upload']              = 'Upload required!';
			$_['text_upload']               = 'Your file was successfully uploaded!';
			
$_['text_reviews']             = '%s reviews';
$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews for this product.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_related']             = 'Related Products';
$_['text_tags']                = 'Tags:';
$_['text_error']               = 'Product not found!';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'half-month';
$_['text_month']               = 'month';
$_['text_year']                = 'year';

				$_['text_review']  = 'Отзыв';
				

// Entry
$_['entry_qty']                = 'Qty';
$_['entry_name']               = 'Your Name';
$_['entry_review']             = 'Your Review';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Tabs
$_['tab_description']          = 'Description';
$_['tab_attribute']            = 'Specification';
$_['tab_review']               = 'Reviews (%s)';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
