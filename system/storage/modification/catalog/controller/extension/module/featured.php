<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerExtensionModuleFeatured extends Controller {
	public function index($setting) {

                $this->document->addStyle('catalog/view/javascript/promotionlabelpro/style.css');
                
		$this->load->language('extension/module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

                $this->load->model('catalog/promotion_label_product');
                

		$data['products'] = array();

	
	//opencart turbo start	
		$cache_data = null;
		
		if ($this->config->get('turbo_featured_module') && $this->config->get('turbo_status')) {
			$cache = 'turbo.featured_module.' . $this->language->get('code') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->session->data['currency'] . '.' . (int)$this->config->get('config_customer_group_id') . '.'  . substr(md5(serialize($setting)), 0, 8);
			$cache_data = $this->cache->get($cache);
		}
			
		if (!empty($cache_data) && is_array($cache_data)) {
			$data['products'] = $cache_data;
		} else {	
	//opencart turbo end
	

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}


                $labels = array();
                $labels = $this->model_catalog_promotion_label_product->getProductLabel($product_info['product_id']);
                
					$data['products'][] = array(

                'labels'   => $labels,
                
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}


		//opencart turbo start	
			if ($this->config->get('turbo_featured_module') && $this->config->get('turbo_status')) {
				$this->cache->set($cache, $data['products']);
			};
					
			}
		//opencart turbo end 		
	
		if ($data['products']) {
			return $this->load->view('extension/module/featured', $data);
		}
	}
}