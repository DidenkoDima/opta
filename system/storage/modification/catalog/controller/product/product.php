<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('product/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
$data['video_status'] = $this->config->get('video_status');

              
            $data['config'] = $this->config;
            $data['customer'] = $this->customer;
            $data['request'] = $this->request; 
              
			if ($this->config->get('product_reviews_status') && $this->config->get('product_reviews_language_status')) {
				$this->load->model('catalog/review');

				$product_info['reviews'] = $this->model_catalog_review->getTotalReviewsByProductId($product_info['product_id'], $this->config->get('config_language_id'));

				if (!$product_info['reviews']) {
					$product_info['reviews'] = $this->model_catalog_review->getTotalReviewsByProductId($product_info['product_id'], false);
				}
			}
			
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			if ($product_info['meta_title']) {
				$this->document->setTitle($product_info['meta_title']);
			} else {
				$this->document->setTitle($product_info['name']);
			}
			
			if ($product_info['noindex'] <= 0) {
				$this->document->setRobots('noindex,follow');
			}
			
			if ($product_info['meta_h1']) {	
				$data['heading_title'] = $product_info['meta_h1'];
			} else {
				$data['heading_title'] = $product_info['name'];
			}
			
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');

		    	$this->document->addOGMeta('property="og:url"', $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']) );
                
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');

			$this->load->model('catalog/review');

			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['model'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

				if ($product_info['image']) {
					$this->document->addOGMeta('property="og:image"', str_replace(' ', '%20', $this->model_tool_image->resize($product_info['image'], 600, 315)) );
					$this->document->addOGMeta('property="og:image:width"', '600');
					$this->document->addOGMeta('property="og:image:height"', '315');
			    } else {
		    		$this->document->addOGMeta( 'property="og:image"', str_replace(' ', '%20', $this->model_tool_image->resize($this->config->get('config_logo'), 300, 300)) );
					$this->document->addOGMeta('property="og:image:width"', '300');
					$this->document->addOGMeta('property="og:image:height"', '300');
	     		}
				foreach ($results as $result) {
			    	$this->document->addOGMeta( 'property="og:image"', str_replace(' ', '%20', $this->model_tool_image->resize($result['image'], 600, 315)) );
					$this->document->addOGMeta('property="og:image:width"', '600');
					$this->document->addOGMeta('property="og:image:height"', '315');
       			}
                

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
'video' => $result['video'],
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
				);
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['special'] = false;
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
				);
			}

			$data['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}


			if ($this->config->get('product_reviews_status')) {
				$this->document->addScript('catalog/view/javascript/jrating-master/jquery/jrating.jquery.js');
				$this->document->addStyle('catalog/view/javascript/jrating-master/jquery/jrating.jquery.css');

				$data['entry_add_pros'] = $this->language->get('entry_add_pros');
				$data['entry_add_cons'] = $this->language->get('entry_add_cons');
				$data['entry_review_image'] = sprintf($this->language->get('entry_review_image'), $this->config->get('product_reviews_image_limit'));
				$data['entry_review_title'] = $this->language->get('entry_review_title');
				$data['entry_recommend_product'] = $this->language->get('entry_recommend_product');
				$data['text_product_review'] = sprintf($this->language->get('text_product_review'), $product_info['name']);
				$data['text_general_avarage'] = $this->language->get('text_general_avarage');

				if ($this->config->get('product_reviews_recommend_status')) {
					$recomend_info = $this->model_catalog_review->getRecommendProductId($product_info['product_id']);

					$recommend_yes = (int)$recomend_info['yes'];
					$recommend_total = (int)$recomend_info['total'];
					$recommend_percent = ($recommend_total > 0) ? round(($recommend_yes / $recommend_total) * 100) : 0;
				} else {
					$recommend_yes = 0;
					$recommend_total = 0;
					$recommend_percent = 0;
				}

				$data['text_count_recommend_product'] = sprintf($this->language->get('text_count_recommend_product'), $recommend_yes, $recommend_total, $recommend_percent . '%');
				$data['recommend_total'] = $recommend_total;
				$data['text_general_count_mark'] = sprintf($this->language->get('text_general_count_mark'), $product_info['reviews']);
				$data['text_please_wait'] = $this->language->get('text_please_wait');
				$data['text_report_abuse'] = $this->language->get('text_report_abuse');
				$data['text_other_reason'] = $this->language->get('text_other_reason');
				$data['text_sort'] = $this->language->get('text_sort');
				$data['text_yes'] = $this->language->get('text_yes');
				$data['text_no'] = $this->language->get('text_no');
				$data['error_logged_helpfull'] = $this->language->get('error_logged_helpfull');
				$data['error_logged_report_abuse'] = $this->language->get('error_logged_report_abuse');
				$data['button_write_review'] = $this->language->get('button_write_review');

				$data['href_report_abuse'] = $this->url->link('product/allreviews/reportabuse', '', 'SSL');

				$reasons = $this->model_catalog_review->getReasonsTitle();

				if ($reasons) {
					$data['reasons'] = $reasons;
				} else {
					$data['reasons'] = array();
				}

				$data['ratings'] = $this->model_catalog_review->getRatings();
				$data['total_ratings'] = $this->model_catalog_review->getRatingsByProductId($product_id);
				$data['predefined_pros'] = $this->model_catalog_review->getPredefinedProsCons(array('filter_type' => '1'));
				$data['predefined_cons'] = $this->model_catalog_review->getPredefinedProsCons(array('filter_type' => '0'));

				$data['product_review_sorts'] = array();

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'r.date_added-DESC'
				);

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC'
				);

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC'
				);

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_helpfull_desc'),
					'value' => 'vote-DESC'
				);

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_helpfull_asc'),
					'value' => 'vote-ASC'
				);

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_date_added_desc'),
					'value' => 'r.date_added-DESC'
				);

				$data['product_review_sorts'][] = array(
					'text'  => $this->language->get('text_date_added_asc'),
					'value' => 'r.date_added-ASC'
				);

				if (version_compare(VERSION, '2.0') < 0) {
					$this->data = array_merge($this->data, $data);
				}
			}
			

				$meta_price = ( $data['special'] != false) ? $data['special'] : $data['price'] ;
				$meta_price = trim(trim(($data['special'] != false) ? $data['special'] : $data['price'], $this->currency->getSymbolLeft($this->session->data['currency'])), $this->currency->getSymbolRight($this->session->data['currency']));
				$decimal_point_meta_price = $this->language->get('decimal_point') ? $this->language->get('decimal_point') : '.';
                $thousand_point_meta_price = $this->language->get('thousand_point')? $this->language->get('thousand_point') : ' ';
                $meta_price = str_replace($thousand_point_meta_price, '', $meta_price);
                if ( $decimal_point_meta_price != '.' ) {
                  $meta_price = str_replace($decimal_point_meta_price, '.', $meta_price);
                }
                $meta_price = number_format((float)$meta_price, 2, '.', '');
				$this->document->addOGMeta('property="product:price:amount"', $meta_price);
				$this->document->addOGMeta('property="product:price:currency"', $this->session->data['currency']);
                
			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}

			$data['share'] = $this->url->link('product/product', 'product_id=' . (int)$this->request->get['product_id']);

			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			$data['products'] = array();

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}

			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

				$data['file_to_product'] = $this->load->controller('file/file'); 
			
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$data['product_tabs']=array();
			
			$tabresults = $this->model_catalog_product->getproducttab($this->request->get['product_id']);
			
			foreach($tabresults as $result){
				$data['product_tabs'][]=array(
					'product_tab_id' => $result['product_tab_id'],
					'title'   => $result['heading'],
					'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
				);
			}


				
				$this->language->load('file/file'); 
				$data['tab_file'] = $this->language->get('tab_file');
				$data['config_file_block_positions'] = $this->config->get('config_file_block_positions');
				$data['config_file_block_custom_positions_block'] = $this->config->get('config_file_block_custom_positions_block');
			
			$this->response->setOutput($this->load->view('product/product', $data));
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

				$data['file_to_product'] = $this->load->controller('file/file'); 
			
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function review() {

			$this->load->language('product/product');

			if (isset($this->request->get['sort'])) {
				$sort_data = explode('-', $this->request->get['sort']);

				if (isset($sort_data[1]) && preg_match('/asc|desc/i', $sort_data[1])) {
					$sort = $this->request->get['sort'];
				} else {
					$sort = 'r.date_added-DESC';
				}
			} else {
				$sort = 'r.date_added-DESC';
			}

			$data['text_pros'] = $this->language->get('text_pros');
			$data['text_cons'] = $this->language->get('text_cons');
			$data['text_reply'] = $this->language->get('text_reply');
			$data['text_on'] = $this->language->get('text_on');
			$data['text_average_review'] = $this->language->get('text_average_review');
			$data['text_report_it'] = $this->language->get('text_report_it');
              
            $data['config'] = $this->config;
            $data['customer'] = $this->customer;
            $data['request'] = $this->request; 

			if (version_compare(VERSION, '2.0') < 0) {
				$this->data = array_merge($this->data, $data);
			}

			$this->load->model('tool/image');

			$language_id = ($this->config->get('product_reviews_status') && $this->config->get('product_reviews_language_status')) ? (int)$this->config->get('config_language_id') : false;
			
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id'], $language_id);

			if (!$review_total) {
				$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id'], false);
				$language_id = false;
			}
			

$limit = ($this->config->get('product_reviews_status') && $this->config->get('product_reviews_limit')) ? (int)$this->config->get('product_reviews_limit') : 5;
		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * $limit, $limit, $language_id, $sort);

		foreach ($results as $result) {

			$pros = $this->model_catalog_review->getProsByReviewId($result['review_id']);
			$cons = $this->model_catalog_review->getConsByReviewId($result['review_id']);

			if ($result['vote_yes']) {
				$helpfull = round($result['vote_yes'] / ($result['vote_yes'] + $result['vote_no']) * 100);
			} else {
				$helpfull = '0';
			}

			$review_images = array();

			if ($this->config->get('product_reviews_image_status')) {
				foreach ($this->model_catalog_review->getReviewImages($result['review_id']) as $image) {
					if ($image['image'] && file_exists(DIR_IMAGE . 'product_review/review/' . $image['image'])) {
						$review_images[] = array(
							'thumb' => $this->model_tool_image->resize('product_review/review/' . $image['image'], $this->config->get('product_reviews_image_thumb_width'), $this->config->get('product_reviews_image_thumb_height')),
							'popup' => $this->model_tool_image->resize('product_review/review/' . $image['image'], $this->config->get('product_reviews_image_popup_width'), $this->config->get('product_reviews_image_popup_height'))
						);
					}
				}
			}

			$comment_images = array();

			if ($this->config->get('product_reviews_image_status')) {
				foreach ($this->model_catalog_review->getReviewCommentImages($result['review_id']) as $image) {
					if ($image['image'] && file_exists(DIR_IMAGE . 'product_review/review/' . $image['image'])) {
						$comment_images[] = array(
							'thumb' => $this->model_tool_image->resize('product_review/review/' . $image['image'], $this->config->get('product_reviews_image_thumb_width'), $this->config->get('product_reviews_image_thumb_height')),
							'popup' => $this->model_tool_image->resize('product_review/review/' . $image['image'], $this->config->get('product_reviews_image_popup_width'), $this->config->get('product_reviews_image_popup_height'))
						);
					}
				}
			}
			
			$data['reviews'][] = array(

			'review_id'          => $result['review_id'],
			'review_title'       => $result['title'],
			'images'             => $review_images,
			'comment_images'     => $comment_images,
			'comment'            => ($this->config->get('product_reviews_comment_status')) ? $result['comment'] : '',
			'comment_date_added' => date($this->language->get('date_format_short'), strtotime($result['comment_date_added'])),
			'product_pros'       => ($pros) ? $pros : array(),
			'product_cons'       => ($cons) ? $cons : array(),
			'ratings'            => $this->model_catalog_review->getRatingsByReviewId($result['review_id']),
			'share_url'          => $this->url->link('product/allreviews', 'review_id=' . (int)$result['review_id']),
			'share_title'        => str_replace('"', '', sprintf($this->language->get('text_share_title'), $result['author'], $result['name'])),
			'helpfulness'        => ($this->config->get('product_reviews_helpfulness_type') == 'numerically') ? sprintf($this->language->get('text_helpfull_numerically'), $result['review_id'], $result['review_id'], (int)$result['vote_yes'], ((int)$result['vote_yes'] + (int)$result['vote_no'])) : sprintf($this->language->get('text_helpfull_percentage'), $result['review_id'], $result['review_id'], $helpfull . '%'),
			
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}&sort=' . $sort);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));


			if ($this->config->get('product_reviews_status')) {
              
                $data['config'] = $this->config;
                $data['customer'] = $this->customer;
                $data['request'] = $this->request;              
              
				if (version_compare(VERSION, '2.0') < 0) {
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/apr_review.tpl')) {
						$this->template = $this->config->get('config_template') . '/template/product/apr_review.tpl';
					} else {
						$this->template = 'default/template/product/apr_review.tpl';
					}
					$this->response->setOutput($this->render());
				} else {
                    $this->response->setOutput($this->load->view('product/apr_review', $data));
				}
				return;
			}
			
		$this->response->setOutput($this->load->view('product/review', $data));
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			if ($this->config->get('product_reviews_status')) {
				$this->load->model('catalog/review');

				if ($this->config->get('product_reviews_rating_guest') && !$this->customer->isLogged()) {
					$json['error'] = $this->language->get('error_logged_guest_rate');

					$this->response->setOutput(json_encode($json));

					return;
				}

				$empty = array_filter($this->request->post['rating']);

				if (empty($this->request->post['rating']) || (count($empty) != count($this->request->post['rating']))) {
					$json['error'] = $this->language->get('error_rating');
				}

				if ($this->config->get('product_reviews_captcha') && $this->customer->isLogged()) {
					$this->request->post['captcha'] = $this->session->data['captcha'];
				}

				if ($this->config->get('product_reviews_purchase_status') && $this->customer->isLogged()) {
					if (!$this->model_catalog_review->productPurchasedByCustomer($this->request->get['product_id'], $this->customer->getId())) {
						$json['error'] = $this->language->get('error_purchase_product');
					}
				}

				if ($this->config->get('product_reviews_limit_product_status') && $this->customer->isLogged()) {
					if ($this->model_catalog_review->alreadyWrittenByCustomer($this->request->get['product_id'], $this->customer->getId())) {
						$json['error'] = $this->language->get('error_already_review_product');
					}
				}

				if ($this->config->get('product_reviews_review_title_status')) {
					$this->request->post['review_title'] = strip_tags($this->request->post['review_title']);

					if ((utf8_strlen($this->request->post['review_title']) < 3) || (utf8_strlen($this->request->post['review_title']) > 40)) {
						$json['error'] = $this->language->get('error_review_title');
					}
				}

				if ($this->config->get('product_reviews_pros_cons_character_limit_from') && $this->config->get('product_reviews_pros_cons_character_limit_to')) {
					$from = (int)$this->config->get('product_reviews_pros_cons_character_limit_from');
					$to = (int)$this->config->get('product_reviews_pros_cons_character_limit_to');

					if (isset($this->request->post['review_pros']) && $this->request->post['review_pros']) {
						foreach (array_filter($this->request->post['review_pros']) as $pros) {
							if (utf8_strlen($pros) < $from || utf8_strlen($pros) > $to) {
								$json['error'] = sprintf($this->language->get('error_pros_cons_limit'), $from, $to);

								break;
							}
						}
					}

					if (isset($this->request->post['review_cons']) && $this->request->post['review_cons']) {
						foreach (array_filter($this->request->post['review_cons']) as $cons) {
							if (utf8_strlen($cons) < $from || utf8_strlen($cons) > $to) {
								$json['error'] = sprintf($this->language->get('error_pros_cons_limit'), $from, $to);

								break;
							}
						}
					}
				}
			}
			
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				
			if ($this->config->get('product_reviews_status')) {
				//$json['error'] = $this->language->get('error_rating');
			}
			
			}

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					
			if ($this->config->get('product_reviews_status') && ($this->config->get('product_reviews_captcha') && $this->customer->isLogged())) {
				//$json['error'] = $this->language->get('error_captcha');
			} else {
				$json['error'] = $this->language->get('error_captcha');
			}
			
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->load->language('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
