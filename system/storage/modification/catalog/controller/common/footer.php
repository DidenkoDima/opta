<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonFooter extends Controller {
	public function index() {

            	$this->load->language('product/search');
                $data['text_empty'] = $this->language->get('text_empty');

            	$data['text_view_all_results'] = $this->config->get('live_search_view_all_results')[$this->config->get('config_language_id')]['name'];
                $data['live_search_ajax_status'] = $this->config->get('live_search_ajax_status');
                $data['live_search_show_image'] = $this->config->get('live_search_show_image');
                $data['live_search_show_price'] = $this->config->get('live_search_show_price');
                $data['live_search_show_description'] = $this->config->get('live_search_show_description');
                $data['live_search_href'] = $this->url->link('product/search', 'search=');
                $data['live_search_min_length'] = $this->config->get('live_search_min_length');
            
		$this->load->language('common/footer');

			$data['lang_id'] = $this->config->get('config_language_id');
			$data['callbackpro'] = $this->config->get('callbackpro');
			$data['callbackpro']['positions'] = array(
              '1' => 'bottom:auto; left:38px; right:auto; top:38px;',
              '2' => 'bottom: 45%; left:38px; right:auto; top: 45%;',
              '3' => 'bottom:38px; left:38px; right:auto; top:auto;',
              '4' => 'bottom:auto; left:auto; right:38px; top:38px;',
              '5' => 'bottom: 45%; left:auto; right:38px; top: 45%;',
              '6' => 'bottom:38px; left:auto; right:38px; top:auto;',
            );	
			

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		//opencart turbo start
		
		$cache_data = null;
		
		if ($this->config->get('turbo_information') && $this->config->get('turbo_status')) {
			$cache = 'turbo.footer_information.' . $this->language->get('code') . '.' . $this->config->get('config_store_id');
			$cache_data = $this->cache->get($cache);
		}
		
		if (!empty($cache_data) && is_array($cache_data)) {	
			$data['informations'] = $cache_data;	
		} else {	
		//opencart turbo end 
	

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}


		//opencart turbo start
			if ($this->config->get('turbo_information') && $this->config->get('turbo_status')) {
				$this->cache->set($cache, $data['informations']);
			}
		}
		//opencart turbo end 
	
		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

				$this->load->model('setting/setting');
				$current_language_id = $this->config->get('config_language_id');
				$data['loadmore_button'] = $this->config->get('loadmore_button_name_'.$current_language_id);
				$data['loadmore_status'] = $this->config->get('loadmore_status');
				$data['loadmore_style'] = $this->config->get('loadmore_style');
				$data['loadmore_arrow_status'] = $this->config->get('loadmore_arrow_status');
            

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		return $this->load->view('common/footer', $data);
	}
}
