var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    browserSync = require("browser-sync"),
    themeName = 'default';

var path = {    
    build: {     
        css: '../catalog/view/theme/'+themeName+'/css/',
    },
    src: {    
        style: '../catalog/view/theme/'+themeName+'/scss/*.scss',
    },
    watch: {
        style: '../catalog/view/theme/'+themeName+'/scss/*.scss',
    },
};
// var path = {    
//     build: {     
//         css: '../scss/',
//     },
//     src: {    
//         style: '../scss/*.scss',
//     },
//     watch: {
//         style: '../scss/*.scss',
//     },
// };

// var config = {
//     proxy: "http://k2trade/",
//     tunnel: true,
//     host: 'localhost',
//     port: 3000,
//     logPrefix: "Frontend_Work"
// };
gulp.task('style:build', function () {
    gulp.src(path.src.style) 
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        // .pipe(reload({stream: true}));
        .pipe(browserSync.reload({stream: true}));
});
gulp.task('build', [
    'style:build',
]);

gulp.task('watch', function(){
    gulp.watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    // gulp.watch('../catalog/view/theme/'+themeName+'/stylesheet/*.css', browserSync.reload);

    // gulp.watch('../catalog/view/theme/'+themeName+'/template/**/*.tpl', browserSync.reload);
    // gulp.watch('../catalog/view/theme/'+themeName+'/js/**/*.js', browserSync.reload);
    // gulp.watch('../catalog/view/theme/'+themeName+'/libs/**/*', browserSync.reload);
    
});

// gulp.task('webserver', function () {
//     browserSync(config);
// });

// gulp.task('start', ['build', 'watch', 'webserver']);
gulp.task('start', ['build', 'watch']);